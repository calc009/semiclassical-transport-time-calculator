#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jul  4 13:46:43 2019

@author: mja
"""
#import math
import numpy as np
import csv 
import scipy.integrate as spi
#import scipy.integrate as spi#.integrate as integ
import os
from subprocess import call
import subprocess
from os import listdir
from os.path import isfile, join, isdir
import fnmatch
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit
#from scipy.signal import argrelextrema
import matplotlib.ticker as ticker
from mpl_toolkits.axes_grid1 import make_axes_locatable
from scipy.optimize import curve_fit
#import colormaps as cmaps
from scipy.signal import argrelextrema
from datetime import date
import time

#import differentiation as dif
##################################################


##########################################################
def filter_by_checklist(OrigLs,CheckLs):
  Ls1=[];
  for f in OrigLs:
    for ch in CheckLs:
      if ch in f:
        Ls1.append(f)
  return Ls1
###################################################################
###################################################################

TTdir="TravelTimeMCtr/"
TTfile="TotTimeSClas_"
FiguDir="Figures/"
TText=".dat"
deg2rad=np.pi/180.0
Ninterm=5
#checkstr=["W6Se14","W7Se12"]#["W7Se12"]#["W6Se14"]#["W7Se12"]#["W6Se14"]#["W7Se12"]#["W7Se12","W6Se14"]
print(listdir(TTdir))
Ls0 = [TTdir+f+"/" for f in listdir(TTdir) if isdir( TTdir+f ) ] #if f in checkstr  ]
print(Ls0)
#Ls1=[f for f in Ls0 if checkstr ]
#Ls1=filter_by_checklist(Ls0,checkstr)
#print(Ls1)
Ls1=Ls0



#inpaux=file0[0].split("_")[1] #
#molec=inpaux.split("-")[0]
#print(molec)
#print("type(molec)",type(molec))
# import sys 
fecha=date.today()
hora=time.localtime( )
horacio=time.strftime("%Y%M%d_%H%M%S")
#timestringsucho=#str(fecha)+"-"+t.hour+t.minute+t.second
#timestringsucho=horacio
#customname=cust+str(timestringsucho)


EPhotExpt=90.0
colorvec=["k","r","b","y","g","m","c"]
stylevec=["-",".",".-","--"]
au2fs=0.02418884
ev2au=27.2113845
twopi=2.0*3.1415926535897932384626
clight=137.036

######################################################################
#NameLs=[]
#AllLs=[]
for Ls in Ls1:
  print(Ls+TTdir)
  #AllLs+=[f+f1 for f1 in listdir(f) if isfile( f+f1 ) if TTfile in f1 ]# if TTfile in f1 ]
  AllLs=[Ls+f1 for f1 in listdir(Ls) if isfile( Ls+f1 ) if TTfile in f1 ]# if TTfile in f1 ]
  #print(LsAux)
  nme=Ls.split("/")
  print(nme)
  clusname=nme[1]
  #print(AllLs)  


  #SubLs= [[None]]*(Ninterm+1) 
  SubLs=[[] for i in np.arange(Ninterm+1)]
  print(SubLs)
  print(SubLs[0])
  #SubLs[0].append("B")
  print(SubLs)
  for fl in AllLs:
    print(fl)
    split0=fl.split(TTfile)[1]
    print(split0)
    split1=split0.split(TText)[0]
    print(split1)
    wpLs=split1.split("-")
    print(wpLs)
    Nwp=len(wpLs)
    print("Nwp",Nwp)
    idx=Nwp-2
    print(len(SubLs[idx]))
    SubLs[idx].append(fl)
  
  
  LenLs=[[] for i in range(len(SubLs))]
  #for L in LenLs:
  #  print(L)
  #  
    #nEn=len(set0)
  for iL,subLs in enumerate(SubLs):
    print(Ls[0])
    set0=np.loadtxt(subLs[0])
    print(set0.shape)
    #print( len(Ls))
  
  LimLs=[]
  for iL,subLs in enumerate(SubLs):
    print(subLs[0])
    set0=np.loadtxt(subLs[0])
    print(set0.shape)
    #### Assuming the same energy grid, skipping the need for interpolation.
    mn=np.zeros_like(set0)
    mx=np.zeros_like(set0);
    mx[:,0]=set0[:,0];
    mn[:,0]=set0[:,0]; mn[:,1]=1000000.0
    for L in subLs:
      set1=np.loadtxt(L)
      mn[:,1]=np.where(mn[:,1]<set1[:,1],mn[:,1],set1[:,1])
      
      mx[:,1]=np.where(mx[:,1]>set1[:,1],mx[:,1],set1[:,1])
    LimLs.append( [mn,mx])
  #print(SubLs[0])
  #print(SubLs[2][14])
  #print(SubLs[0])
  #print(SubLs[0][14])
    #np.loadtxt(fl)
    
  #print(SubLs," eeeeeah")
  #print(SubLs[2])

####################################################################


  plt.gcf().clear()
  
  #plt.gca().set_aspect('equal') ## 
  
  fig = plt.figure(figsize=(10, 7.5), dpi=80)
  fig.set_frameon(False)
  
  ax1 = fig.add_subplot(111)
  #ax4=fig.add_subplot(111) 
  
  
  ax1.set_ylabel('C. T. delay [as]', fontsize = 35)
  ax1.set_xlabel('Photon Energy [eV]', fontsize = 35)
  ax1.set_xlabel('Kinetic Energy [eV]', fontsize = 35)
  #ax4.set_ylabel('Time delay [as]', fontsize = 35)
  #ax4.set_xlabel('Photon Energy [eV]', fontsize = 35)
  
  
  percent=5.0
  xmin=10.0
  xmax=90.0
  xstep=20.0
  xmaxPLT=xmax+percent/100.0*(xmax-xmin)
  ymin=0.0#30.0#65.0#100.0#30.0#60.0
  ymax=200.0#50.0#95.0#60.0#100.0
  ystep=50.0
  #ax5=ax1.twiny().twinx()
  #ax4=ax1.twiny()#.twinx()
  ax1.set_xlim(xmin,xmaxPLT)
  ax1.set_xticks(np.arange(xmin, xmax+xstep, xstep))
  ax1.set_ylim(ymin,ymax)
  ax1.set_yticks(np.arange(ymin, ymax+ystep, ystep))
  ax1.tick_params("both",labelsize=35)
  #ax2.tick_params("x",labelsize=35)
  #ax1.set_axis_off()
  
  
  #ax4.set_xlim(xmin,xmax)
  #ax4.set_ylim(ymin,ymax)
  #ax4.set_xticks(np.arange(xmin, xmax+xstep, xstep))
  #ax4.set_yticks(np.arange(ymin, ymax+ystep, ystep))
  #ax4.tick_params("both",labelsize=35)
  
  #cmpalette=plt.cm.jet
  #ax1=plt.tricontourf(spec[:,0],spec[:,1],spec[:,2], 50,cmap=cmpalette)
  #plt.savefig("Spectrum.png" )#,bbox_inches='tight', transparent=False)
  
  for iH,Hits in enumerate(LimLs):#=[]
  
    mn=Hits[0]
    mx=Hits[1]
  
    ax1.plot(mn[:,0],mn[:,1],color=colorvec[iH+1], \
     linestyle="-", linewidth=4,label=" Total collisions : "+str(iH+1))
    ax1.plot(mx[:,0],mx[:,1],color=colorvec[iH+1], \
     linestyle="-", linewidth=4)#,label=clusname)
    ax1.fill_between(mx[:,0],mn[:,1],mx[:,1],color=colorvec[iH+1],alpha=0.3)
      
  # for ifi, fil in enumerate(FileLs):
  #   icolor=ifi%len(colorvec)
  #   clusname=fil.split("_")[0]
  #   aux=np.loadtxt(fil)
  #   orbname=fil.split("_")[2].split(".")[0]
  #   
  #   
  #   x3,y3=eliminate_peaks_doublets(aux[:,0],aux[:,1],wdpts=6 )
  #   np.savetxt("TESTfilter-"+clusname+"-"+orbname,np.transpose([x3,y3] ))
  #   tdtab=np.asarray(x3)#np.asarray(x2)
  #   COE=y3#y2#[
  #   ax4.plot(tdtab,COE,color=colorvec[icolor], \
  #   linestyle="-", linewidth=4,label=clusname)
  
  # leg4=ax4.legend(fontsize = 26,frameon=False, loc=2,ncol=2)#loc=(0.45,0.0))
  
  leg1=ax1.legend(fontsize = 26,frameon=False, loc='upper right',ncol=1)#loc=(0.45,0.0))
  # for line,text in zip(leg4.get_lines(), leg4.get_texts()):
  #     text.set_color(line.get_color())
  #     
  figname="SCtimePlotKE_"+clusname+".png"
  
  plt.savefig(FiguDir+ figname,bbox_inches='tight',\
        transparent=False)

  print("Exported figure : ", FiguDir+ figname)


  print("Found Ninterm :", Ninterm, clusname)