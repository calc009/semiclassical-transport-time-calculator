#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 02:47:38 2020

@author: jax
"""
import numpy as np
import scipy.interpolate as spi
import scipy.integrate as spt
from . import basics as bas
from . import initialstates as ini
from . import import_data as imp

wd0=1.5
def calc_weighted_statdensity(Energrid,EbndLs0,coeff0):
	assert(len(EbndLs0)==len(coeff0))
	EbndLs=np.asarray(EbndLs0)
	coeff=np.asarray(coeff0)
	dens=np.zeros(len(Energrid),dtype=float)
	for iE,Ebd in enumerate(EbndLs):#Eisolved.BndEner:
			if Ebd > 0.0:
				dens+=coeff[iE]*add_EistateDens(Energrid,Ebd)#*bas.eV_to_au)
	return dens


def calc_statdensity(Energrid,EbndLs): ### all in eV
	dens=np.zeros(len(Energrid),dtype=float)
	for Ebd in EbndLs:#Eisolved.BndEner:
			if Ebd > 0.0:
				dens+=add_EistateDens(Energrid,Ebd)#*bas.eV_to_au)
	return dens
def add_EistateDens(Energrid,EbdEv,wd=wd0):
	dens1=np.exp(-(Energrid-float(EbdEv))**2/(2*wd**2))
	return dens1

class rel_vecs():
	def __init__(self,vec0,vecf,index0,indexf,start_cyl=0,end_cyl=0,\
   Energrid=np.linspace(10.0,150.0,351)):
		pass
		self.vec0=vec0
		self.vecf=vecf
		self.vecrel=vecf-vec0
		
		self.vlen=np.sqrt( np.dot(self.vecrel,self.vecrel))
		self.n0=300
		self.make_grid0()
		self.make_grid03D()
		self.indexLs=[index0,indexf]
		print("Instantiated with :", self.indexLs)
		self.Energrid=Energrid
		self.EnergridAU=self.Energrid/bas.eV_to_au
		self.nEn=len(self.Energrid)
		self.StatesDensCoeffDirec()
		#self.end_cyl=Coulinp.AtCatalog[indexf]["IRadCyl"]
		#self.start_cyl=Coulinp.AtCatalog[index0]["IRadCyl"] 
		self.start_cyl=start_cyl
		self.end_cyl=end_cyl
		self.outermost_cyl=max(self.end_cyl, self.start_cyl)
				
	def closest_perp_dist_to_atom(self,AtomCatalog):
		
		assert(isinstance(AtomCatalog,imp.ImportedData))
# 						auxDct={"index":iat,"species":Spe,"charge":Zchar,"car":atCart2 ,\
# 						"sph":atSph,"cyl":atCyl,"special":[]}
# 				#self.AtCatalogDictio[iat]=auxDct
# 				#print("dictio aux",auxDct)
# 				self.AtCatalog.append(auxDct)
		dperpCat=[]
		for At in AtomCatalog.AtCatalog:
			if At["index"] in self.indexLs:
				continue
			l0,lf,dperp,withzero=bas.identify_l0lfy(At["car"],self.vec0,self.vecf)
			### this time, I need withzero==True
			if withzero==True:
				dperpCat.append(dperp)
			else :
				dperpCat.append(self.vlen)
		#if len(dperpCat)>=1:
		CSrad=min(dperpCat)#min(min(dperpCat),self.vlen)
		#else:
		#	CSrad=self.vlen
		self.CSradius=CSrad
		#return 
	def dist_relvec_to_point(self, point=np.array([0.0,0.0,0.0]))	:
		pass
	#def dist_point_to_point(point1,point2):
	#	difvec=point1-point2
	#	d=np.dot(difvec)
	#	return d
	def calc_my_StatesDens(self): ## after calc_eigenstates
		pass
		self.ResoEnLs 
		self.my_StatesDens={}
		
		for mk in self.coeffDirecDictio.keys():#["all"]
			self.my_StatesDens[mk]=np.zeros_like(self.Energrid)
			for Econt in self.ResoEnLs: #ResoEnLs vienen en eV.
				self.my_StatesDens[mk]+=\
		self.coeffDirecDictio[mk]*add_EistateDens(self.Energrid,Econt,wd=wd0)
#	def add_EistateDens(Energrid,EbdEv,wd=2.5):
#		dens1=np.exp(-(Energrid-float(EbdEv))**2/(2*wd**2))
#		return dens1
				
	def StatesDensCoeffDirec(self,kvers=np.array([0.0,0.0,1.0])):
		self.coeffDirecDictio={}
		kversloc=kvers/np.sqrt(np.dot(kvers,kvers))
		self.coeffDirecDictio["emdir"]=( np.dot( self.vecrel,kversloc )/self.vlen)**2
		self.coeffDirecDictio["perplane"]=1.0-self.coeffDirecDictio["emdir"]**2
		self.coeffDirecDictio["all"]=1.0
		
	def make_grid0(self):
		self.grd0=np.zeros((self.n0),dtype=float)
		
		for i in np.arange(self.n0):
			h=i/(self.n0-1)
			self.grd0[i]=self.vlen*h
		#self.grd0=np
	def make_grid03D(self):
		self.grid03D=np.zeros( (self.n0,3) , dtype=float)
		
		for i in np.arange(self.n0):
			h=i/(self.n0-1)
			self.grid03D[i]=self.vec0+h*self.vecrel
	def make_interpolated_potential(self,Vobj):
		assert isinstance(Vobj,spi.RegularGridInterpolator)
		self.Vgrid0=Vobj(self.grid03D)
		self.Vintegral=spt.simps(self.Vgrid0, x=self.grd0)
		
	def calc_eigenstates(self):
		pass
		self.Eisolved=ini.InitialStates(self.Vgrid0,self.grd0,bc="box",nsten=7,calcBnd=True,nEn=70)
		self.Eisolved.create_bnd_states()
		self.ResoEnLs=[]
		for En in self.Eisolved.BndEner:
			if En>0.0 :
				self.ResoEnLs.append(En*bas.eV_to_au)
		self.ResoEnAU=np.asarray(self.ResoEnLs)/bas.eV_to_au
	def calc_statdensity(self):
		self.dens=np.zeros(len(self.Energrid),dtype=float)
		for Ebd in self.ResoEnLs:#Eisolved.BndEner:
			if Ebd > 0.0:
				self.add_EistateDens(Ebd)#*bas.eV_to_au)
	def add_EistateDens(self,EbdEv,wd=0.5):
		self.dens+=np.exp(-(self.Energrid-float(EbdEv))**2/(2*wd**2))
		
	def calc_classicaltime(self):
		self.TauVecClass=np.zeros(len(self.EnergridAU),dtype=float)
		for iEn,En in enumerate(self.EnergridAU):
			kvec=np.where(En-self.Vgrid0>0.2,\
								 np.sqrt(2.0*( En-self.Vgrid0 ) ), 100000.0 )
			assert( len(kvec)==len(self.Vgrid0) )
			self.TauVecClass[iEn]=spt.simps(1.0/kvec,self.grd0)*bas.fs_to_au*1000.0
			
	def calc_semiclassicaltime(self):
		self.TauVecSClass=np.zeros(len(self.EnergridAU),dtype=float)
		self.TauVecSClassEival=np.zeros(len(self.ResoEnAU),dtype=float)
		for iEn,En in enumerate(self.EnergridAU):
			kvec=np.where(En-self.Vgrid0>0.1,\
								 np.sqrt(2.0*( En-self.Vgrid0 ) ), 100000.0 )
			assert( len(kvec)==len(self.Vgrid0) )
			## get the closest iBnd
			iBd=np.argmin(np.abs( self.Eisolved.BndEner-En) )
			if self.Eisolved.BndEner[iBd]-En >0.0 :
				iBd2=iBd+1
				iBd1=iBd
				E2=self.Eisolved.BndEner[iBd2]
				E1=self.Eisolved.BndEner[iBd1]
			elif	self.Eisolved.BndEner[iBd]-En <=0.0 :
				iBd2=iBd
				iBd1=iBd-1
				E2=self.Eisolved.BndEner[iBd2]
				E1=self.Eisolved.BndEner[iBd1]
			self.TauVecSClass[iEn]=(En-E1)*spt.simps(\
				np.abs(self.Eisolved.bndStates[:,iBd2])**2/kvec,self.grd0)\
								*bas.fs_to_au*1000.0 +\
					(E2-En)*spt.simps(np.abs(self.Eisolved.bndStates[:,iBd1])**2/kvec,self.grd0)\
				*bas.fs_to_au*1000.0
			self.TauVecSClass[iEn]/=(E2-E1)
#			self.TauVecSClass[iEn]=spt.simps(np.abs(self.Eisolved.bndStates[:,iBd])**2/kvec,self.grd0)\
#				*bas.fs_to_au*1000.0 
		for iEn,En in enumerate(self.ResoEnAU):
			kvec=np.where(En-self.Vgrid0>0.1,np.sqrt(2.0*( En-self.Vgrid0 ) ), 100000.0 )
			iBd=np.argmin(np.abs( self.Eisolved.BndEner-En) )
			self.TauVecSClassEival[iEn]=spt.simps(np.abs(self.Eisolved.bndStates[:,iBd])**2/kvec,self.grd0)\
				*bas.fs_to_au*1000.0 