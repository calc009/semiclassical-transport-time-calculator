import numpy as np
atomicNumbers = {1:'H', 2:"He", 3: "Li", 4:"Be", 5:"B", 6:"C",\
		 7: "N", 8:"O", 9:"F", 10:"Ne", 11:"Na", 12:"Mg",\
		 13:"Al", 14:"Si", 15:"P", 16:"S", 17:"Cl",\
			  18:"Ar",74:"W",34:"Se",40:"Mo" }
localTol=1e-10

eV_to_au=27.2113845;  angs_to_au=0.529177249
fs_to_au=0.02418884; wm2_to_au=3.51e+16 


def roundcyph(x,n):
	y=round( x*10**n,10**n)/10**n
	return y

def directory_check_create(dirname,clear=True):
	import subprocess as sbp
	from os.path import isfile, isdir
	existFILE=isfile(dirname)
	if existFILE:
		print (dirname+" exists as a file, not a directory!")
		return 
	existsDIR=isdir(dirname)
	if existsDIR==True:
		if clear==True:
			sbp.call( "rm -rf "+dirname ,shell=True)
			sbp.call( "mkdir "+dirname ,shell=True)
	else :	
		sbp.call( "mkdir "+dirname ,shell=True)
	
	return
####################################################
def cosgamma(v1,v2):
	dt=np.dot(v1,v2)
	mod1=np.sqrt(np.dot(v1,v1))
	mod2=np.sqrt(np.dot(v2,v2))
	cos=dt/mod1/mod2
	return cos
######################################################
def get_dist_to_top(ztop,s1,s2):
	pass

######################################################

# def get_integrated_distance_lineardec(ref,s1,s2,kappa):
# 	if len(ref)==1:
# 		l0,lf,dperp=identify_l0lfy(ref[0],s1,s2)
# 		D=integrated_dist(dperp,l0,lf)
# 	if len(ref)==2:
# 		l0,lf,dperp=identify_l0lfy(ref[0],s1,s2)
# 		D1=integrated_dist(dperp,l0,lf)
# 		l0,lf,dperp=identify_l0lfy(ref[1],s1,s2)
# 		D2=integrated_dist(dperp,l0,lf)
# 		D=min(D1,D2)
# 	return D

def Ifull(x0,y,l):
	I0=distance_primitive(l,y,itype=0,kappa=0.0)
	I1=distance_primitive(l,y,itype=1,kappa=0.0)
	I2=distance_primitive(l,y,itype=2,kappa=0.0)
	Ifu=(x0**2+y**2)*I0 - 2*x0*I1 + I2
	Denom=x0**2*l-2*x0*I0+I1	
	return Ifu, Denom
######################################################
def get_integrated_dens_parab(ref,s1,s2,kappa):
	if len(ref)==1:
		D=get_integrated_dens_point_parab(ref[0],s1,s2,kappa)
	if len(ref)==2:
		#l0,lf,dperp,withzero=identify_l0lfy(ref[0],s1,s2)
		D1=get_integrated_dens_point_parab(ref[0],s1,s2,kappa)
		#l0,lf,dperp,withzero=identify_l0lfy(ref[1],s1,s2)
		D2=get_integrated_dens_point_parab(ref[1],s1,s2,kappa)
		D=D1+D2 #min(D1,D2) ### porque tener dos centros es
		###  mas longitud abarcada, a diferencia de la dint
	return D
######################################################
def get_integrated_dens_point_parab(p,s1,s2,kappa):
	l0,lf,dperp,withzero=identify_l0lfy(p,s1,s2)
	x0=3.0/kappa
	A=1.0/x0**2
	if np.sign(l0*lf)>0.0 and np.abs(l0) > x0 and np.abs(lf) > x0:
		return 0.0
	if np.abs(l0)>x0:
		l0=np.sign(l0)*x0 ## check this in sep prog
	if np.abs(lf)>x0:
		lf=np.sign(lf)*x0 ## check this in sep prog

	if withzero==True:
		D1f,Denom1f=Ifull(x0,dperp,lf)
		D00,Denom00=Ifull(x0,dperp,0.0)
		D10,Denom10=Ifull(x0,dperp,l0)
		Denom1=Denom1f -Denom00 
		Denom2=Denom10-Denom00
		Denom=A*(Denom1+Denom2)
	else :
		D1f,Denomf=Ifull(x0,dperp,lf)
		D10,Denom0=Ifull(x0,dperp,l0)
		Denom=A*(Denomf-Denom0)
	return Denom
	
######################################################
def get_integrated_distance_to_point_parab(p,s1,s2,kappa):
	l0,lf,dperp,withzero=identify_l0lfy(p,s1,s2)
	x0=3.0/kappa
	A=1.0/x0**2

	if np.sign(l0*lf)>0.0 and np.abs(l0) > x0 and np.abs(lf) > x0:
		return x0

	if np.abs(l0)>x0:
		l0=np.sign(l0)*x0 ## check this in sep prog
	if np.abs(lf)>x0:
		lf=np.sign(lf)*x0 ## check this in sep prog

	if withzero==True:
		D1f,Denom1f=Ifull(x0,dperp,lf)
		D00,Denom00=Ifull(x0,dperp,0.0)
		D10,Denom10=Ifull(x0,dperp,l0)
		D1=A*(D1f -D00 )
		Denom1=A*(Denom1f -Denom00 )
		D2=A*( D10-D00 )
		Denom2=A*(Denom10-Denom00)
		D=(D1+D2)
		Denom=(Denom1+Denom2)
	else :
		D1f,Denomf=Ifull(x0,dperp,lf)
		D10,Denom0=Ifull(x0,dperp,l0)
		D=A*(D1f-D10)
		Denom=A*(Denomf-Denom0)
		
		
	return D/Denom

######################################################
def get_integrated_distance_parab(ref,s1,s2,kappa):
	if len(ref)==1:
		D=get_integrated_distance_to_point_parab(ref[0],s1,s2,kappa)
	if len(ref)==2:
		#l0,lf,dperp,withzero=identify_l0lfy(ref[0],s1,s2)
		D1=get_integrated_distance_to_point_parab(ref[0],s1,s2,kappa)
		#l0,lf,dperp,withzero=identify_l0lfy(ref[1],s1,s2)
		D2=get_integrated_distance_to_point_parab(ref[1],s1,s2,kappa)
		D=D1+D2 #min(D1,D2) ### porque tener dos centros es
		###  mas longitud abarcada, a diferencia de la dint
	return D
######################################################
def get_integrated_distance_to_point(p,s1,s2):
	l0,lf,dperp,withzero=identify_l0lfy(p,s1,s2)
	if withzero==True:
		D1=integrated_dist(dperp,l0,0.0)
		D2=integrated_dist(dperp,0.0,lf)
		D=D1+D2
	else :
		D=integrated_dist(dperp,l0,lf)
	return D
###########################################################

def get_integrated_distance(ref,s1,s2):
	if len(ref)==1:
		D=get_integrated_distance_to_point(ref[0],s1,s2)
	if len(ref)==2:
		#l0,lf,dperp,withzero=identify_l0lfy(ref[0],s1,s2)
		D1=get_integrated_distance_to_point(ref[0],s1,s2)
		#l0,lf,dperp,withzero=identify_l0lfy(ref[1],s1,s2)
		D2=get_integrated_distance_to_point(ref[1],s1,s2)
		D=min(D1,D2)
	return D

###########################################################
def integrated_dist(y,l0,lf):
	lfl=max(lf,l0)
	l0l=min(l0,lf)
	intdist=distance_primitive(lfl,y)-distance_primitive(l0l,y)
	return intdist/(lf-l0)
############################################################
def distance_primitive(l,y,itype=0,kappa=0.0):
	if itype==0:
		if abs(y)<1e-4:
			dp=0.5*l*np.sqrt(l**2+y**2)
		else:
			dp=0.5*l*np.sqrt(l**2+y**2)+0.5*y**2*np.log( np.abs(l+np.sqrt(l**2+y**2)) )
	elif itype==1:
		dp=y**2*l+l**3/3.0
	elif itype==2:
		if abs(y)<1e-4:
			dp0=y**2*l +2*l**3
			dp1=  np.sqrt(y**2+l**2)*dp0 /8.0
			dp=dp1
		else:
			dp0=y**2*l-y**3*np.arcsinh(l/y)/np.sqrt(1.0+l**2/y**2) +2*l**3
			dp1=  np.sqrt(y**2+l**2)*dp0 /8.0
			dp=dp1
		
	#elif itype==3:
		
	return dp
############################################################
def calc_distance_to_ref(ref, segment):
	if len(ref)==1:
		dist=distance_point_to_segment(ref[0],segment)
	elif len(ref)==2:
		dist=distance_segment_to_segment(ref,segment)
	return dist
#############################################################
def distance_point_to_segment(pvec,svecs,imod=0):## pvec es un vector, no una lista de un elem.
	dls=[]
	for s in svecs:
		dls.append(np.sqrt(np.dot( s-pvec,s-pvec) ) )
	l0,lf,dperp,withzero=identify_l0lfy(pvec,svecs[0],svecs[1])
	print(min(dls),dperp,max(dls))
	if withzero==True:
		dmin=dperp
	else :
		dmin=min(dls);
	dmax=max(dls)	
	return dmin,dmax#surf=max(dls)
##########################################################
def distance_segment_to_segment(svecs1,svecs2,imod=0):### svecs1 es lista de dos vecs
	dls=[]
	for s1 in svecs1:
		d1,d2=distance_point_to_segment(s1,svecs2,imod=0)
		#dls.append(np.sqrt(np.dot( s1-s2,s1-s2) ) )
		dls.append(d1); dls.append(d2)
	dmin=min(dls); dmax=max(dls)
	return dmin,dmax
##########################################################
def identify_l0lfy(p,s1,s2):
	cross=np.cross(s1-p ,s2-s1 )
	num2=np.dot(cross,cross) #np.cross(s1-p ,s2-p )
	den2=np.dot(s2-s1 ,s2-s1)
	dperp=np.sqrt(num2/den2)
	
	u=(s2-s1)/np.sqrt(den2)
	l1=np.dot(s1-p,u)
	l2=np.dot(s2-p,u)
	l0=min(l1,l2)
	lf=max(l1,l2)
	
	if np.sign(l0*lf)<0 :
		withzero=True
	else :
		withzero=False
		
	return l0,lf,dperp,withzero

####################################################
def elimreps_sublist(lst):
	nonreps=[]
	for ls in lst:
		if nonreps==[]:
			nonreps.append(ls)
			continue
		rep=False
		for nls in nonreps: 
			print(ls,"==",nls,"?",ls==nls)
			if nls==ls:
				rep=True
				break
				#print("broken?")
		if rep==False:
			nonreps.append(ls)
		#if ls not in nonreps:
		#	
	return nonreps

def elimrep(lst):
	nonreps=[]
	for ls in lst:
		if ls not in nonreps:
			nonreps.append(ls)
	return nonreps

def elim_chars(lst,elimLs):
	nonreps=[]
	#last=""
	for ls in lst:
		if ls not in elimLs:
			nonreps.append(ls)
	return nonreps

def make_coord_grid_1D(mn,step,Nsteps):
	print(mn,step,Nsteps)
	grd=np.empty( Nsteps, dtype=float)
	for i in np.arange(Nsteps):
		grd[i]=mn+step*i
	return grd

def to_spher(cart):
	spher=np.zeros(3,dtype=float)
	
	#0: r, 1: theta, 2: phi.
	spher[0]=np.sqrt(np.dot(cart,cart))
	if spher[0]<localTol:
		spher[1]=0.0#np.arccos( cart[2]/spher[0] )
	else	:
		spher[1]=np.arccos( cart[2]/spher[0] )
	spher[2]=np.arctan2( cart[1],cart[0]  )
	
	return spher 

def to_cyl(cart):
	cyl=np.zeros(3,dtype=float)

	#0: rcyl, 1: phi, 2: z.
	cyl[0]=np.sqrt(np.dot(cart[0:2],cart[0:2])) #i.e., excluding z comp
	cyl[1]=np.arctan2( cart[1],cart[0]  )# phi, same as in spher.
	cyl[2]=cart[2] #z 

	return cyl 
