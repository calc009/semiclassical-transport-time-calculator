#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Jul  1 16:43:12 2019

@author: mja
"""
import numpy as np


def make_derivec_single(x,y,nsten=5):
	
	dy=np.empty_like(y)
	npts=len(y)
	#shvec=dy.shape
	#nd=y.ndim
	#assert(nd==2)
	#nf=shvec[1]; npts=shvec[0]
	tp=type(dy[0])
		
	q=(nsten-1)//2
	xlc=np.zeros(nsten, dtype= tp )
	
	xlc[:]=x[0:nsten]
	for i in np.arange(q):
		dvec=filad(xlc,nsten,i)
		
		dy[i]=np.dot(dvec , y[0:nsten] )
	
	for i in np.arange(q,npts-q):
		xlc[:]=x[i-q:i+q+1 ]
		dvec=filad(xlc,nsten,q)
		#for ifu in np.arange(nf):
		dy[i]=np.dot(dvec, y[i-q:i+q+1] )

	i0=npts-nsten
	i1=npts-q
	xlc[:]=x[i0:npts ]
	for i in np.arange(i1,npts):
		dvec=filad(xlc,nsten, q+i-i1)
		#for ifu in np.arange(nf):
		dy[i]=np.dot(dvec, y[npts-nsten:npts] )

	return dy

def make_derivec(x,y,nsten=5):
	
	dy=np.empty_like(y)
	#npts=len(y)
	shvec=dy.shape
	nd=y.ndim
	assert(nd==2)
	nf=shvec[1]; npts=shvec[0]
	tp=type(dy[0,0])
		
	q=(nsten-1)//2
	xlc=np.zeros(nsten, dtype= tp )
	
	xlc[:]=x[0:nsten]
	for i in np.arange(q):
		dvec=filad(xlc,nsten,i)
		for ifu in np.arange(nf):
			dy[i,ifu]=np.dot(dvec , y[0:nsten,ifu] )
	
	for i in np.arange(q,npts-q):
		xlc[:]=x[i-q:i+q+1 ]
		dvec=filad(xlc,nsten,q)
		for ifu in np.arange(nf):
			dy[i,ifu]=np.dot(dvec, y[i-q:i+q+1,ifu] )

	i0=npts-nsten
	i1=npts-q
	xlc[:]=x[i0:npts ]
	for i in np.arange(i1,npts):
		dvec=filad(xlc,nsten, q+i-i1)
		for ifu in np.arange(nf):
			dy[i,ifu]=np.dot(dvec, y[npts-nsten:npts,ifu] )

	return dy

def filad2(xlc,nsten,nu):

	d2=np.zeros(nsten)
	subset=np.arange(nsten)

	for i1 in subset:

		auxi2=0.0
		for i2 in subset:
			if i2==i1:
				continue
			auxi3=0.0
			for i3 in subset:
				if i3==i1:
					continue
				if i3==i2:
					continue
				auxi4=1.0
				for i4 in subset:
					if i4==i1:
						continue
					if i4==i2:
						continue
					if i4==i3:
						continue
					auxi4=auxi4*(xlc[nu]-xlc[i4])/(xlc[i1]-xlc[i4])
				auxi3=auxi3+auxi4/(xlc[i1]-xlc[i3])

			auxi2=auxi2+auxi3/(xlc[i1]-xlc[i2])
		d2[i1]=auxi2
	return d2


def filad(xlc,nsten,nu):

	d1=np.zeros(nsten)
	subset=np.arange(nsten)

	for i1 in subset:
		auxi2=0.0
		for i2 in subset:
			if i2==i1:
				continue

			auxi3=1.0
			for i3 in subset:
				if i3==i1:
					continue
				if i3==i2:
					continue
				auxi3=auxi3*(xlc[nu]-xlc[i3])/(xlc[i1]-xlc[i3])

			auxi2=auxi2+auxi3/(xlc[i1]-xlc[i2])
		d1[i1]=auxi2
	return d1