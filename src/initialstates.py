#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jun 19 14:28:20 2019

@author: mja

"""
from os import listdir
from os.path import isfile, join, isdir
#########################################
import numpy as np
import scipy.integrate as spi
import scipy.linalg as sli
import scipy.sparse as spa
import scipy.special as spe
#################################
from . import differentiation as dif
from . import basics as bas
#from . import 

##===============================================## 

########################################################	
class InitialStates():

	def __init__(self,U,grid,bc="box",nsten=3,calcBnd=True,nEn=30,normhow="tog"):
		
		#self.grid=None
		#self.pote=None
		self.nsten=nsten
		#self.xL=None
		#self.xR=None
		self.calcBnd=True
		#self.bndStates=None ### esto tiene que ser un flor de array luego
		#self.bndEners=None ### array de
		#self.nEn=None
		#self.bndStatesD=None
		self.normhow=normhow
		self.U=U
		self.grid=grid
		self.bc=bc
		self.npts=grid.shape[0]
		self.nsten=nsten
		self.nEn=nEn
		self.qsten=(nsten-1)//2
		if self.bc=="box":
			self.xL=np.zeros(1)
			self.xR=np.zeros(1)
			self.xL[0]=self.grid[0]-(self.grid[1]-self.grid[0])
			self.xR[0]=self.grid[self.npts-1]+\
				(self.grid[self.npts-1]-self.grid[self.npts-2])
				
		#if self.bc=="trubox":
			
		print(self.xL,self.xR)
		
		

	def create_bnd_states(self):

		q=(self.nsten-1)//2
		npts=self.npts
		nsten=self.nsten
		#Hop=-0.5/dx**2*spa.diags(d2, np.linspace(-qsten,qsten,nsten).astype(int),shape=(npts,npts),dtype=complex  )
		#for
		#### armado de las diags:
		nelem=0
		for iq in np.arange(-q,q+1,dtype=int):
			nelem+=npts-np.abs(iq)
		for iq in np.arange(1,q):
			nelem+=2*iq # los puchitos que quedan en las diags de bc.
		data=np.zeros(nelem,dtype=complex)
		indices=np.zeros(nelem,dtype=int)
		indptr=np.zeros(npts+1,dtype=int)

		indices[0]=0 ## por esta porqueria los tres loops de abajo se desplazan
		### en +1.

		for irow in np.arange(0,q+1):#npts+1): ### las primeras
			indptr[irow+1]=indptr[irow]+nsten-1
			indices[indptr[irow]:indptr[irow+1]]=\
				np.arange(0,nsten-1,dtype=int)
			xlc=np.concatenate( (self.xL,self.grid[0:nsten-1]) )
			vec0=-0.5*self.filad2(xlc,nu=irow+1)[0:nsten]
			vec1=-0.5*self.filad2(xlc,nu=irow+1)[1:nsten]
			#vec1[0]+=vec0[0]  # esto hace cond de deriv cero en lado iz.
			data[indptr[irow]:indptr[irow+1]]=	vec1
			
			#-0.5*self.filad2(xlc,nu=q)[1:nsten]
			#	-0.5*self.filad2(xlc,nu=irow+1)[1:nsten]
			
				#self.filad2(irow= ,nu)
			###filad2(self,i,nu)
			#print(self.filad2(xlc,nu=irow+1))


		for irow in np.arange(q+1,npts-q):  ### el medio
			indptr[irow+1]=indptr[irow]+nsten
			#print( irow,npts,nelem, indptr[irow],indptr[irow+1] )
			#print( np.arange(irow-q,irow+q+1,dtype=int) )
			#print(indices[indptr[irow]:indptr[irow+1]].shape, np.arange(irow-q,irow+q+1,dtype=int).shape)
			indices[indptr[irow]:indptr[irow+1]]=np.arange(irow-q,irow+q+1,dtype=int)
			xlc=self.grid[irow-q:irow+q+1]
			data[indptr[irow]:indptr[irow+1]]=\
				-0.5*self.filad2(xlc,nu=q)

			#print(self.filad2(i=q,nu=q))

		for irow in np.arange(npts-q,npts): ### las finales
			indptr[irow+1]=indptr[irow]+nsten-1
			indices[indptr[irow]:indptr[irow+1]]=np.arange( npts-nsten+1,npts,dtype=int)
			xlc=np.concatenate((self.grid[self.npts-nsten+1:self.npts], self.xR) )
			iaux=-npts+irow ##un index que va al reves...
			print("iaux", iaux)
			data[indptr[irow]:indptr[irow+1]]=\
				-0.5*self.filad2(xlc,nu= nsten+iaux  )[0:nsten-1]

			print(self.filad2(xlc,nu= nsten+iaux  ))

		Hop=spa.csr_matrix((data, indices, indptr), shape=(npts,npts))+\
			spa.csr_matrix(spa.spdiags(self.U, diags=[0],m=npts,n=npts) )


		#indptr = np.array([0, 2, 3, 6])
###indices = np.array([0, 2, 2, 0, 1, 2])
###data = np.array([1, 2, 3, 4, 5, 6])
###csr_matrix((data, indices, indptr), shape=(3, 3)).toarray()

		#W,vr=spa.linalg.eigs(Hop, k=neival,which="SR",M=Vgen,Minv=VgenInv)### funciona
		self.BndEner,self.bndStates=spa.linalg.eigs(Hop, k=self.nEn,which="SR" )### funciona
		self.nrm=np.zeros(self.nEn,dtype=float)
		if self.normhow=="sep":
			for iEn  in np.arange(self.nEn):
				if np.real(self.BndEner[iEn]) < 0.0:
					self.nrm[iEn]=spi.simps(np.abs(self.bndStates[:,iEn])**2,self.grid)
					self.bndStates[:,iEn]=self.bndStates[:,iEn]/np.sqrt(self.nrm[iEn])
				else :
					i0z=np.argmin(np.abs(self.grid))
					imax=np.argmax( np.abs( self.bndStates[i0z:,iEn]) )
					self.nrm[iEn]=np.abs(self.bndStates[imax,iEn])
					self.bndStates[:,iEn]=self.bndStates[:,iEn]/self.nrm[iEn]
		elif self.normhow=="tog":
			for iEn  in np.arange(self.nEn):
				
				self.nrm[iEn]=spi.simps(np.abs(self.bndStates[:,iEn])**2,self.grid)
				self.bndStates[:,iEn]=self.bndStates[:,iEn]/np.sqrt(self.nrm[iEn])


		
		self.bndStatesD=np.empty_like(self.bndStates)
		self.bndStatesD=dif.make_derivec(self.grid ,self.bndStates,nsten=5)
		self.EnerWidth=self.make_enerwidths(self.BndEner)
		self.TWidth=1.0/self.EnerWidth*bas.fs_to_au*1000.0*bas.eV_to_au
########################################################################
	@staticmethod
	def make_enerwidths(BndEner,imod=0):
		
		nEn=len(BndEner)
		EnerWidth=np.zeros(nEn,dtype=float)
		if imod==0:
			for iEn, En in enumerate( BndEner):
				if En>0.0 :
					if iEn==0:
						wd=0.5*float( BndEner[iEn+1] -BndEner[iEn] )
					elif iEn==nEn-1:
						wd=0.5*float( BndEner[iEn] -BndEner[iEn-1] )
					else:
						wd=0.25*float( BndEner[iEn+1] -BndEner[iEn-1] )
				else :
					wd=1000.0
				EnerWidth[iEn]=wd
		if imod==1:
			
			for iEn, En in enumerate( BndEner):
				if En>0.0 :
					if iEn==0:
						wd=0.5*float( BndEner[iEn+1] -BndEner[iEn] )
					elif iEn==nEn-1:
						wd=0.5*float( BndEner[iEn] -BndEner[iEn-1] )
					else:
						wd=0.5*min( float( BndEner[iEn+1] -BndEner[iEn] ),float( BndEner[iEn] -BndEner[iEn-1] ) )
				else:
					wd=1000.0
				EnerWidth[iEn]=wd
		return EnerWidth
###################################################################################

	def filad2(self,xlc,nu):
		assert(self.grid is not None)
#		x=self.grid
		nsten=self.nsten
#		q=self.qsten
		d2=np.zeros(nsten)
		subset=np.arange(nsten)
#		if (self.bc=="box"):
#			#assert(i-q>=-1)
#			#assert(i+q<=self.npts)
#			if i<=q:
#				#print(i,"equis?",x[0:nsten-1] , "xL?", self.xL)
#				xlc=np.concatenate( (self.xL,x[0:nsten-1]) )
#			elif i>=self.npts-q:
#				xlc=np.concatenate((x[self.npts-nsten+1:self.npts], self.xR) )
#				print(i,xlc.size)
#			else:
#				xlc=x[i-q:i+q+1]
#				#print(i,xlc.size)
		for i1 in subset:

			auxi2=0.0
			for i2 in subset:
				if i2==i1:
					continue
				auxi3=0.0
				for i3 in subset:
					if i3==i1:
						continue
					if i3==i2:
						continue
					auxi4=1.0
					for i4 in subset:
						if i4==i1:
							continue
						if i4==i2:
							continue
						if i4==i3:
							continue
						auxi4=auxi4*(xlc[nu]-xlc[i4])/(xlc[i1]-xlc[i4])
					auxi3=auxi3+auxi4/(xlc[i1]-xlc[i3])

				auxi2=auxi2+auxi3/(xlc[i1]-xlc[i2])
			d2[i1]=auxi2
		return d2


	def filad(self,xlc,nu):
		assert(self.grid is not None)

		#x=self.grid
		nsten=self.nsten
#		q=self.qsten
		d1=np.zeros(nsten)
		subset=np.arange(nsten)

#		if (self.bc=="box"):
#			assert(i-q>=-1)
#			assert(i+q<=self.npts)
#			if i<=q:
#				xlc=np.concatenate((self.xL,x[0:nsten-1]))
#			elif i>self.npts-q:
#				xlc=np.concatenate((x[self.npts-nsten+1,self.npts], self.xR) )
#			else:
#				xlc=x[i-q:i+q]

		for i1 in subset:
			auxi2=0.0
			for i2 in subset:
				if i2==i1:
					continue

				auxi3=1.0
				for i3 in subset:
					if i3==i1:
						continue
					if i3==i2:
						continue
					auxi3=auxi3*(xlc[nu]-xlc[i3])/(xlc[i1]-xlc[i3])

				auxi2=auxi2+auxi3/(xlc[i1]-xlc[i2])
			d1[i1]=auxi2
		return d1









