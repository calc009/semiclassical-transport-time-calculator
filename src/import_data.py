import numpy as np
from . import basics as bas
#from . import molec_data as mda
import pandas as pd
#import pandas_datareader as pdr

# class InterpolatedPotentials():
# 	def __init__(self,relvecLs,):
# 		self.relvecLs
atomicNumbers = {1:'H', 2:"He", 3: "Li", 4:"Be", 5:"B",\
		6:"C", 7: "N", 8:"O", 9:"F", 10:"Ne", 11:"Na", 12:"Mg",\
			 13:"Al", 14:"Si", 15:"P", 16:"S", 17:"Cl", 18:"Ar",\
				 34:"Se",74:"W"}

#electroneg=pdr.data.DataReader("data/electroneg.dat")
electroneg=pd.read_csv("data/electroneg.dat",sep="\t")
electroneg.set_index("Symbol",inplace=True,drop=False)
#electronegSy=electroneg.set_index("Symbol")
print(electroneg.columns)
#DataReader(t, data_source="yahoo",start="2012-1-1", \
#	   end="2016-12-31")["Adj Close"]
#import sys 
#sys.exit("me fui")

class ImportedData():
	
	def __init__(self,fname,eleccharge=-1.0):
		self.fname=fname
		self.eleccharge=eleccharge
		if "XC" in fname:
			self.typ="XC"
		elif "Coul" in fname:
			self.typ="Coul"
		self.cylradtol=0.05
		self.read_ADFPotential_header()
		
		
		
		
	def read_ADFPotential_header(self):
		fread=open(self.fname,"r")
		self.AtCatalog=[]
		#self.AtCatalogDictio={}
		atCart=np.zeros(3,dtype=float)
		#atSph=np.zeros_like(atCart)
		#atCyl=np.zeros_like(atCart)
		#PosDictio={}
		iat=-1
		#special=[]
		for il,l in enumerate(fread,0):
			print(il,l)
			if l.isspace():
				continue
			sep0=l.split()
			if il==0:
				self.title=l 
			elif il==1: 
				self.xtra=l
			elif il==2:
				
				self.Nat=int(sep0[0])
				self.Xmin=float(sep0[1])
				self.Ymin=float(sep0[2])
				self.Zmin=float(sep0[3])
				print("Nat", self.Nat)
			elif il==3: 
				#sep0=l.split()
				self.NX=int(sep0[0])
				self.Xstep=float(sep0[1])
			elif il==4: 
				#sep0=l.split()
				self.NY=int(sep0[0])
				self.Ystep=float(sep0[2])				
			elif il==5: 
				#sep0=l.split()
				self.NZ=int(sep0[0])
				self.Zstep=float(sep0[3])				
			elif il>=6 and il <self.Nat+6:
				#sep0=l.split()
				Zchar=int(sep0[0])
				Spe=bas.atomicNumbers[Zchar]
				print("colecting data from",l)
				print("colecting data from",sep0)
				for iq in np.arange(3):
					atCart[iq]=float(sep0[2+iq])#/bas.angs_to_au
				print("read effin pos?", atCart)
				atSph=bas.to_spher(atCart)
				atCyl=bas.to_cyl(atCart)
				atCart2=1*atCart
				#PosDictio["car"]=atCart
				#PosDictio["sph"]=atSph
				#PosDictio["cyl"]=atCyl
				iat+=1
				#self.AtCatalog.append([iat,Spe,Zchar, PosDictio,special ])
				#self.AtCatalog.append([iat,Spe,Zchar, PosDictio,special ])
				auxDct={"index":iat,"species":Spe,"charge":Zchar,"car":atCart2 ,\
						"sph":atSph,"cyl":atCyl,"special":[]}
				#self.AtCatalogDictio[iat]=auxDct
				#print("dictio aux",auxDct)
				self.AtCatalog.append(auxDct)
				
				#print("dictio aux appended",self.AtCatalog[-1])
				#print("len so far? ",len(self.AtCatalog))
				print("car appendeado", self.AtCatalog[-1]["car"])
			elif il==self.Nat+6:
				break
			#### loop en AtCatalog, determinamos los IRadCyl:
			
		self.allcyl=[]
		for dct in self.AtCatalog:
			self.allcyl.append(dct["cyl"][0])
			
			print("-------------------------")
			print(dct)
		cybuckets=[]
		tempbucket=[]
		for cy in self.allcyl:
			if cybuckets==[]:
				tempbucket.append(cy)
				cybuckets.append(tempbucket)
				continue
			appendeado=False
			for bu in cybuckets:
				
				for cb in bu:
					if np.abs(cb - cy)< self.cylradtol:
						bu.append(cy)
						appendeado=True
						break
					else :
						pass
			if appendeado==False:
				bu=[cy]
				cybuckets.append(bu)
		BucketWalls=[]
		BucketAv=[]
		#for bu in cybukects:
		ibu=np.argsort(cybuckets)
		
		for bu in cybuckets:
			av=np.average(bu)
			BucketAv.append( np.average(bu))
		BucketAv=np.sort(BucketAv)
		for bav in BucketAv:#cybuckets:
			
			BucketWalls.append( [ bav-self.cylradtol ,  bav+self.cylradtol ] )
			
		for dct in self.AtCatalog:
			rcyl=dct["cyl"][0]
			for iwall,walls in enumerate(BucketWalls):
				if rcyl>walls[0] and rcyl<walls[1]:
					dct["IRadCyl"]=iwall
				continue 
			
		self.Pos0Vec=np.array([self.Xmin,self.Ymin,self.Zmin])
		self.StepVec=np.array([self.Xstep,self.Ystep,self.Zstep])
		self.NStepsVec=np.array([self.NX,self.NY,self.NZ ])
		self.make_allcoord_grids_1D()
		self.define_special_traits()
		self.read_array() 
		self.make_meshgrids()
		
		self.count_per_species()
		self.make_molec_name()
		
		
			#print(dct["index"],dct["species"],dct["special"],dct["car"])
	def make_molec_name(self):
		
		self.AtomsDF=pd.DataFrame()
		#self.AtomsDF.columns(["species" ,"Natoms","ENeg","ENegRnk" ] )
		#electroneg.set_index("Symbol")
		for elem in self.PerAtomicSpecies.keys():
			
			print(elem)
			ENeg=electroneg.loc[elem]["Electro-negativity"]#["Electro-negativiy"]
			#ENegRnk=electroneg.loc[elem]["Electro-negativity"]#["Electro-negativiy"]
			
			print("ENeg",ENeg)
			self.AtomsDF=self.AtomsDF.append({"species":elem,\
		"Natoms":self.PerAtomicSpecies[elem], "ENeg":ENeg},ignore_index=True)
		print( self.AtomsDF )
		## Defino y agrego una columna nueva ENegRnk.
		self.AtomsDF["ENegRnk"]=np.argsort(self.AtomsDF["ENeg"])
		print( self.AtomsDF )
			#print(electroneg[elem])
			#pass
		#print("gimme the keys ", electronegSy["W"])
		print("gimme the keys ", electroneg.loc["W"]["Electro-negativity"])

		self.AtomsDF.set_index("ENegRnk",inplace=True,drop=False)
		name=""
		#for irnk in self.AtomsDF["ENegRnk"]:
		#	name+=
		for i in np.arange(len(self.AtomsDF["ENegRnk"])):
			
			if self.AtomsDF.loc[i]["Natoms"]>1:
				nro=str(int(self.AtomsDF.loc[i]["Natoms"]))
			else:
				nro=""
			name+=self.AtomsDF.loc[i]["species"]+nro
		self.MolecName=name
		print(self.MolecName)
		#import sys
		#sys.exit("me fui")
	def count_per_species(self):
		### first run, spot the elements
		#df=pd.DataFrame({"Name":['Tom','Nick','John','Peter'], 
			#   "Age":[15,26,17,28]}) 
		self.PerAtomicSpecies={}#pd.DataFrame()

		#self.PerAtomicSpecies.columns(["species" ,"atoms" ] )
		for dct in self.AtCatalog:
			if dct["species"] not in self.PerAtomicSpecies.keys():#["species"]:
				#new_row = {'species':dct["species"], "atoms":1}
				self.PerAtomicSpecies[dct["species"]]=1
				#self.PerAtomicSpecies.append( ,ignoreIndex=False)[dct["species"]]=1
			else :
				self.PerAtomicSpecies[dct["species"]]+=1
	def define_special_traits(self):
		##this is meant to be used after read_ADFPotential_header
		Rtol=1e-3
		Rctol=1e-3
		Ztol=1e-3
		#self.CTRAX=[]
		Ztop=-100000.0
		### first: run a quick check to get max Z
		print(len(self.AtCatalog))
		for dct in self.AtCatalog:
			print("def special", dct)#["car"][2])
			if dct["car"][2]>Ztop:
				Ztop=dct["car"][2]
		
		print("Ztop",Ztop)
		#for icat,cat in enumerate(self.AtCatalog):
		for dct in self.AtCatalog:
			print("check specials",dct)
			if dct["sph"][0]<Rtol:
				dct["special"]+=["CTR"]
				#self.AtCatalog+=["CTR"]
			elif dct["cyl"][0]<Rctol:
				dct["special"]+=["AX"]
				#
		## para hacer los TOP... tengo que determinar un Ztop.  
			if dct["car"][2]>Ztop-Ztol:
				dct["special"]+=["TOP"]


	def make_allcoord_grids_1D(self):
		self.GridCoordVecs=[]
		for iNN,NN in enumerate(self.NStepsVec,0):
			self.GridCoordVecs.append( bas.make_coord_grid_1D( self.Pos0Vec[iNN], self.StepVec[iNN], NN) )
	def make_meshgrids(self):
		
		self.meshX,self.meshY,self.meshZ= np.meshgrid(*self.GridCoordVecs,indexing="ij",sparse=False )
		self.meshgrids=[self.meshX,self.meshY,self.meshZ]
	def read_array(self):
		Vtab0=np.loadtxt(self.fname,skiprows=self.Nat+6)
		self.Vtab=np.reshape(self.eleccharge*Vtab0,tuple(self.NStepsVec),order="C")
		self.VtabFlat=self.Vtab.flatten(order="C")
