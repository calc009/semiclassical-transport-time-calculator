#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  6 02:51:47 2020

@author: jax
"""
import numpy as np
#import scipy.interpolate as spi
#import scipy.integrate as spt
from . import basics as bas
#from . import initialstates as ini
from . import RelVecs as rel
from . import import_data as imp 


class TravelTime():
	
	def __init__(self,RelVecObj,ImpDatObj,NintermMX=2,TTdir="TravelTimeMCtr/" ):
		self.TTdir=TTdir
		bas.directory_check_create(self.TTdir)
		#self.label=label
		self.NintermMX=NintermMX
		self.RelVecObj=RelVecObj
		self.ImpDatObj=ImpDatObj
		for Rel in self.RelVecObj:
			assert(isinstance(Rel,rel.rel_vecs))
		assert(isinstance(ImpDatObj,imp.ImportedData))
		self.AtCatalog=self.ImpDatObj.AtCatalog
	def make_TravelTimeLs(self):
		pass
	def make_combitos(self,Vectors):#,nintermedMX=self.NintermMX):
		NMX=self.NintermMX+1 #nintermedMX+1
		self.CTRAXLs=[]
		self.TOPLs=[]
		for AtC in self.AtCatalog:
			if "CTR" in AtC["special"]:
				self.CTRAXLs.append(AtC)
			if "AX" in AtC["special"]:
				self.CTRAXLs.append(AtC)
			if "TOP" in  AtC["special"]:
				self.TOPLs.append(AtC)
		self.TimeCombo=[]
		#Waypoints=[]*(NMX+1) #

		#Waypoints=[]*NMX ## eg, sweep over 0,1,2 intermediate stops.
		self.WPFullLs=[]
		for atI in  self.AtCatalog:
			if "CTR" in atI["special"] or "AX" in atI["special"]:
				
				for M in np.arange(NMX): #cada cant de intermedios
					if M==0:
						for atF in self.AtCatalog:
							if "TOP" not in atF["special"]:
								continue
							if atF["index"]==atI["index"]:
								continue
							wpt=[atI["index"]] + [atF["index"]]
							self.WPFullLs.append(wpt)
								
					else:
						med=[ [] for q in range(M)] #[None]*M#len(Waypoints)
						print("shape med", len(med))
						for iM in np.arange(M):
							print("M", M, "iM", iM)
							for atM in self.AtCatalog:
								
								if iM==0:
									if atM["index"]==atI["index"]:
										continue
								elif iM>0:
									if atM["index"]==med[iM-1] :
										continue
								med[iM]=atM["index"]
								for atF in self.AtCatalog:
									if "TOP" not in atF["special"]:
										continue
									
									if atF["index"]==med[-1]:
										continue
									
									if iM==M-1: #once the full med vector is filled, do the writing.
										wpt=[atI["index"]] + med + [atF["index"]]
										self.WPFullLs.append(wpt)
										print("M", M, wpt)
									
			#Waypoints[M].append(M)## un montonazo de cosas
			#pass
				#for pass
		fwrite=open("WPList.dat","w")
		delimiter = '-'
		
		for WP in self.WPFullLs:
			allstring=[str(W) for W in WP if W is not None]
			auxstr=delimiter.join(allstring)#str(WP[:])
			#print(WP)
			#single_str = delimiter.join(auxstr)
			fwrite.write(auxstr+"\n")
		fwrite.close()
	def make_singleTravelTime(self):
		pass
	def pick_one(self,prev,foll):
		pass
	### 
	def make_travel_time_combo(self,RelVecLs):
#		pass #### tengo que levantar los vecs... cada lista wp 
		#### levanta a cada paso un relvec, a identificar por inic/final.
		#fwrite=open("WP-T_List.dat","w")
		
		assert(isinstance(RelVecLs[0],rel.rel_vecs))
		n0=len(RelVecLs[0].TauVecSClass)
		Egrd=RelVecLs[0].Energrid
		#TauVecSClass
		#self.WPTDictio["classical":np.zeros(n0,dtype=float)]
		#self.WPTDictio["semi":np.zeros(n0,dtype=float)]

		delimiter="-"
		self.WPTLs=[]
		for WP in self.WPFullLs:
			allstring=[str(W) for W in WP if W is not None]
			auxstr=delimiter.join(allstring)#str(WP[:])
			fnamC=self.TTdir+"TotTimeClas_"+auxstr+".dat"
			fnamSC=self.TTdir+"TotTimeSClas_"+auxstr+".dat"
			WPDictio={}
			WPDictio["waypoints"]=WP
			WPDictio["classical"]=np.zeros(n0,dtype=float)
			WPDictio["semi"]=np.zeros(n0,dtype=float)
			WPDictio["Energrid"]=Egrd

			for iw,w in enumerate(WP[0:-1]):
				istart=w; iend=WP[iw+1];
				for rv in RelVecLs:
					assert(isinstance(rv,rel.rel_vecs))
					print("inic final de rv ", rv.indexLs)
					if 	rv.indexLs==[istart,iend]: 
						WPDictio["classical"]+=rv.TauVecClass
						WPDictio["semi"]+=rv.TauVecSClass
					elif rv.indexLs==[iend,istart]:
						WPDictio["classical"]+=rv.TauVecClass
						WPDictio["semi"]+=rv.TauVecSClass
					else: 
						pass
						#print("Did not find the travel time between centers ",istart,iend)

			np.savetxt(fnamC,\
			np.transpose( [WPDictio["Energrid"], WPDictio["classical"]]) )
			np.savetxt(fnamSC,\
			np.transpose( [WPDictio["Energrid"], WPDictio["semi"]]) )
				
		#tengo que saber el prev, following, : son los index
		# tengo que saber su special, en el loop para descartar. 
#######################################
