import numpy as np
import os
import subprocess as sbp
from os import listdir
from os.path import isfile, join, isdir

#######################################################
#from . import groupdata as gda

########################################################
eV_to_au=27.2113845

molecSyName={"W12Se14":"D3h","W7Se24":"D3h","W7Se12":"D3h","WSe6":"D3h","W3Se14":"D3h","W6Se14":"D3h", "W3Se2":"D3h","W7Se6":"D3h",\
	"W":"C3v","Se":"C3v"}
molecSpecial={"W12Se14":"AX","W7Se24":"CTR","W7Se12":"CTR",\
		"WSe6":"CTR","W3Se14":"AX","W6Se14":"AX", "W3Se2":"AX","W7Se6":"CTR",\
	"W":"CTR","Se":"CTR"}

BndEnergs={"W4f":-51.0, "Se3d":-64.0, "Se4s":-20.0}

orbEKminEKmax={"Se3d":[70+BndEnergs["Se3d"], 120+BndEnergs["Se3d"]],\
		"Se4s":[30+BndEnergs["Se4s"],110+BndEnergs["Se4s"]],\
			"W4f":[60+BndEnergs["W4f"],120+BndEnergs["W4f"]]}

def calc_kbnd(kwval):
	return (kwval[0],np.sqrt(-2.0*kwval[1]/eV_to_au))
BndKappa=dict(map(calc_kbnd,BndEnergs.items()))

# def fancyname(molec):
# 	
# 	digitchain=""
# 	collect=""
# 	for char in molec:
# 		if char.isdigit():
# 			digitchain=digitchain+char
# 		else :
# 			if digitchain=="":
# 				continue
# 			else:
# 				collect+=r"$"++"$"
# 				digitchain=""
		
#molecIncompleteSym=#{"Se":{"symname":"C3v","ActiveInic":[1,3]},"W" }
molecIncompleteSym={"C3v":{"Se":[1,3]} }
### A dictionary, so you dictionary while you dictonary.
atomicNumbers = {1:'H', 2:"He", 3: "Li", 4:"Be", 5:"B",\
		6:"C", 7: "N", 8:"O", 9:"F", 10:"Ne", 11:"Na", 12:"Mg",\
			 13:"Al", 14:"Si", 15:"P", 16:"S", 17:"Cl", 18:"Ar",\
				 34:"Se",74:"W"}
	
molecAtSt={"W7Se24":{"W4fCTR":["63AA1.1","44AA2.1","104EE.1","104EE.2","48AAA2.1","79EEE.1","79EEE.2"]},\
		#######################################
	"W7Se12":{"W4fCTR":["45AA1.1","26AA2.1","68EE.1","68EE.2","31AAA2.1","45EEE.1","45EEE.2"]},\
	#######################################################
	"W7Se6":{"W4fCTR":["35AA1.1","22AA2.1","54EE.1","54EE.2","21AAA2.1","31EEE.1","31EEE.2"]},\
					########################################
		"W6Se14":{"Se3dAX":["39AA1.1","55EE.1","55EE.2","56EE.1","56EE.2",\
			"25AAA2.1","35EEE.1","35EEE.2","36EEE.1","36EEE.2"],\
		"Se4s":["51AA1.1","52AA1.1","31AA2.1","79EE.1","79EE.2","80EE.1","80EE.2",\
			"21AAA1.1","37AAA2.1","38AAA2.1",\
				"55EEE.1","55EEE.2","56EEE.1","56EEE.2"],\
				"Se4sAX":["51AA1.1","37AAA2.1"],\
				"Se4sMX":["51AA1.1","52AA1.1","37AAA2.1","38AAA2.1"]},\
		######################################################
	"W3Se14": {"Se3dAX":["27AA1.1","38EE.1","38EE.2","39EE.1","39EE.2",\
			"20AAA2.1","28EEE.1","28EEE.2","29EEE.1","29EEE.2"],\
		"Se4s":["36AA1.1","37AA1.1","23AA2.1","56EE.1","56EE.2","57EE.1","57EE.2",\
			"18AAA1.1","29AAA2.1","30AAA2.1",\
				"44EEE.1","44EEE.2","45EEE.1","45EEE.2"],\
				"Se4sAX":["36AA1.1","29AAA2.1"]},\
#############################################################					
"W3Se2":{"Se3d":["18AA1.1","20EE.1","20EE.2","21EE.1",\
											"21EE.2","11AAA2.1",	"10EEE.1","10EEE.2",	"11EEE.1","11EEE.2"],\
								"Se4s":["22AA1.1","15AAA2.1"],\
								},\
#################################################################
"WSe6":{"Se3d":["15AA1.1","16AA1.1","17AA1.1","3AA2.1","4AA2.1",\
											"15EE.1","15EE.2","16EE.1","16EE.2","17EE.1","17EE.2",\
											"18EE.1","18EE.2","19EE.1","19EE.2",\
											"3AAA1.1","4AAA1.1","11AAA2.1","12AAA2.1","13AAA2.1",\
											"12EEE.1","12EEE.2","13EEE.1","13EEE.2",\
											"14EEE.1","14EEE.2","15EEE.1","15EEE.2",\
											"16EEE.1","16EEE.2"],\
								"Se4s":["19AA1.1","16AAA2.1","22EE.1","22EE.2",\
											"18EEE.1","18EEE.2"],\
							"W4f":["18AA1.1","5AA2.1","20EE.1","20EE.2","14AAA2.1","17EEE.1","17EEE.2"]},\
##################################################################								
"Se":{"Se3d":["6A1.1","3E.1","3E.2","4E.1","4E.2"],\
	"Se4s":["7A1.1"]},\
#############################################################
"W":{	"W4f":["11A1.1","12A1.1","1A2.1","8E.1","8E.2","9E.1","9E.2"] },\
	#################################################################
}

	

		
	
	
	