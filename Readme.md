# Approximate resonant state analysis and internal electron propagation time 

## Overall description

This project was a semiclassical electron propagation-time calculation, intended for comparison with full-quantum-mechanical 3D calculations. As it is often the case with computational physics, a very comprehensive and complex simulation makes for a difficult interpretation of the result. 
The main study, published in Ref. [1], addressed the photoelectron propagation throughout a solid as it is being emitted into the continuum, i.e., being released indefinitely from the material by the applied ultra violet radiation. In the cited article the interest rested on the description of each underlying colission or scattering process that gave rise to the observed cross-section and time-delay peaks as functions of the emission angle and kinetic energy. In a nutshelll, the cross section can be thought of an emission probability at a given angle and for a specific emission energy. The Wigner time delay relates to the time the electron spends bouncing around the system before it emerges. The study hinges on analyzing the periodic crystal as an increasingly large cluster of atoms, aiming to find some convergence: how many neighboring atoms do play a role in the photoemission process? How large does a cluster have to get to properly explain the photoemission dynamics that take place in the periodic *infinite* system? The specific subject periodic system in Ref. [1] is WSe$_2$, tungsten diselenide, a transition metal dichalcogenide, and examples of finite clusters employed in said study are WSe$_6$, W$_7$Se$_{12}$, W$_3$Se$_{14}$ and many others. 


Sometimes in quantum mechanics it is possible to observe effects that can translate directly to a classical explanation. This software does calculate classical transport times and weighs them with approximate 1D electron densities of a given positive energy. Considering each involved atom as a scatterer, we work under the hypothesis that an outgoing photoelectron can stay for a while in one or more resonant configuration between any two centers in the atomic cluster configuration. The resonances in this context are essentially energy levels for which the associated particle wavelength fits an integer number of times between two atoms. In such resonant states we can expect the probability amplitude to be significant. A classical interpretation could mean the electron is bouncing back and forth along the line between the two centers, or that there is a high likelihood that forwards or backwards propagation are taking place. In order to obtain the classical travel time along that line we need to know the velocity, which grows with the square root of the available kinetic energy. While inside the solid the electron is subjected to the electric potential of the atomic nuclei and the other electrons, which in total ends up increasing the local kinetic energy. This calculation uses the full 3D potential energy coming from a *density functional theory* prior calculation, and therefore we can interpolate it to any specific 1D trajectory line. The interpolated potentials and the asymptotic kinetic energy, i.e., the leftover energy the electron will end up having after detaching from the solid, are all we need to calculate the travel time between the two endpoints. But we want to go beyond the purely classical: as stated those travel times assume equal probability density along the line, something we know from quantum mechanics not to be the case. That is where we calculate the *allowed* energy states along that 1D line with their spatial probability amplitudes, and we pick the one with its eigenenergy closest to the asymptotic kinetic energy of our photoelectron. To obtain a semiclassical time we do a weighted integration: the time it takes the electron to travel a distance $du$ is $du/v(u)$, i.e., the distance over the local velocity, and we multiply the integrand by the probability density $\rho(u)$. 


Although the number of resonant states for a given outgoing kinetic energy is important, as it would give the photoelectron more optons to stay around inside the solid for a longer interval, we also consider other aspects, such as the center-to-center line alignment with respect to the emission direction, and how wide would a cylinder can get around the center-to-center line before having a third atom across its surface. The former aspect relates to the *post-facto* reasoning that an electron temporarly resonating along a 1D line that is misaligned with the measured emission direction would be less likely collected into the detectors. The latter aspect quantifies how big the available space is for a given (approximate) 1D resonant state, which lets the electron spend more time there before colliding with other atom. 


The code produces a number of physical variables for each possible resonance and tries different weights to ponder their relevance into the cross section shapes in the published article. Ultimately, the analysis that led to the identification of the observable cross-section and time-delay structures came from a comparative  approach. The other information produced by this code is in the form of classical and semiclassical propagation times, with which to have an estimate of how much scattering and rebounds are taking place according to the resulting times from more sophisticated fully quantum mechanical calculations.


## Input

In order to run a calculation for a single system the main code looks into the *ADFPotentials/* *.cube* files for the Coulomb and exchange potentials. It recognizes the strings *XC* and *Coul* for the exchange-potential and Coulomb-potential files respectively. 

## Modules

The module dependency structure was generated with pydeps, and is shown in *src.png*

basics.py : Contains some physical unit-change constants, such as electron-volts to atomic units, or femtoseconds to atomic units, and geometrical-distance calculations. These expressions measure the distance from a point to a segment, segment to segment and there are also integrals of said distances.The module includes basic coordinate conversions, from cartesian to spherical and cylindrical. 

molec_data.py : Stores information about the initial states binding energy, the desired kinetic energy range for each, and their characteristic spatial extension. 

import_data.py  : Contains the *ImportedData* class, which takes care of reading the information from the *.cub* file.

differentiation.py : Includes a number of high-order differentiation routines for arbitrary grids. The methods are based on Lagrange interpolants. 

initialstates.py : Contains the *InitialStates* class, which, is instantiated with a 1D grid and the electrostatic potential at said points to generate Schrodinger Equation eigenstates. It employs a user-defined Lagrangian polynomial order to approximate the derivatives in the discretized coordinate given by the grid, leading to an n-diagonal matrix (introduced in Ref 2) eigenproblem. 

RelVecs.py : Contains the *rel_vecs* class, instantiated with two atom-position vectors and their ordinal indices. It produces attributes that can be derived from their locations the electrostatic potential, supplied as a 3D potential interpolator object. Eigenstates, density of states vs energy and many distance measurements. 

TravelTime.py : The *TravelTime* class is instantiated with an *ImportedData* and a list of *rel_vecs* objects. Given the maximum number of intermediate collisions (default 2), it calculates the semiclassical and classical propagation times between the starting position and the topmost material layer. 

## Main code, supplementary scripts and usage

At the base level there are three python files:
*PostDensf_VeryBDataVlenTsc.py*, *RunAllPotsVeryBigData.py* and *PlotTiemposSemiClassKEonlyAllClusters.py*. The full calculation is scripted by *RunAllPotsVeryBigData.py*, which prepares the input *.cub* files from *ADFPotentialsStorage/* into *ADFPotentials/* and launches the main program *PostDensf_VeryBDataVlenTsc.py*. The plotting is done by launching *PlotTiemposSemiClassKEonlyAllClusters.py* once the computations have been finished. 


## Output

The code exports information to many files, separated by folders, which we describe below.

### StatesDens :  

For each cluster (e.g., W3Se2) and localized orbital (e.g., Se4s) the differently weighed densities of states are exported here. The different weighing options involve the relative vector alignment with respect to the emission direction, the bound-state spatial-decay constant, the minimum distance from a relative vector to a third atom. The different weighing options are in the main file, starting at line 375.

### EivecDepo :

Every single eigenproblem solution is exported to this folder, separated by cluster and within that, by relative vector denoting the ordinals of the starting and ending atoms. The EivecDepo directory contains the collection of all positive resonant eigenenergies for each given cluster and the density of states as a function of the energy. 

### TimeDepo :

For every model cluster, the classical and semiclassical travel times between each possible atom pair (i.e., relative vector). 

### Figures :

This directory contains the graphical results exported by *PostDensf_VeryBDataVlenTsc.py* and *PlotTiemposSemiClassKEonlyAllClusters.py*.

The *Merged* series of plots display resonant states distinguished by alignment, starting-ending atomic species, effective unoccupied radius around that relative vector, and specific atomic perimeter. These were used for comparison with cross sections and Wigner time-delays obtained from the sophisticated fully quantum mechanical calculations of Ref. [1], however, as already stated, the successful physical descriptions came from a different perspective. 

The *SCtime* series of plots display the range of time expected from a different number of undertaken electron collisions during the emission process, separating results per model cluster. This allows to make a semiclassical estimation of how many scattering events an electron experienced. 

### TimeTravelMCtr :

Standing for "travel time, multi-center", this directory stores the compound propagation times (classical and semiclassical) as a function of the kinetic energy, listing in the file name the specific atoms the electron bounced off of. 

## Environment packages

The code has been tested with the Conda environment packages listed in PackagesCondaEnv.txt, generated by ```conda list > PackagesCondaEnv.txt ``` 

## Other requirements

There is a command line call to *montage* tool from ImageMagick to collect graph files, therefore the latter should be installed systemwide. 

## Extensibility 

The module *molec_data.py* contains some localized-orbital and cluster-specific parameters that could be moved to an input file to make the code cluster-agnostic and make the introduction of other system parameters not involve editing the code itself. 

## Reference links

[1] https://journals.aps.org/prb/pdf/10.1103/PhysRevB.105.125405
[2] http://dx.doi.org/10.1063/1.4874115

