#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 30 12:25:43 2020

@author: max
"""

#####################################################
import subprocess as sbp
from os import listdir
#from os.path import isfile, join, isdir
#####################################################

storage="ADFPotentialsStorage/"
runcmd="python PostDensf_VeryBData.py"
runcmd2="python PostDensf6_BData.py"
runcmd3="python PostDensf_VeryBDataVlen.py"
runcmd4="python PostDensf_VeryBDataVlenTsc.py"
#rotate="cp "+"_yyyy_+"/*.cub "+" ADFPotentials/."

MolecList=listdir(storage)

print(MolecList)
for Molec in MolecList:
	rotate="cp "+storage+Molec+"/*.cub "+" ADFPotentials/."
	sbp.call(rotate,shell=True)
	sbp.call(runcmd4,shell=True)
	#sbp.call(runcmd2,shell=True)
	