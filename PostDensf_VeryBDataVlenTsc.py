#import os
import subprocess as sbp
from os import listdir
from os.path import isfile, join, isdir
#import fnmatch
##-------------------------------------
#from itertools import cycle
##------------------------------------
#import math
import numpy as np
#import scipy.special as spe
import scipy.interpolate as spi
#import scipy.integrate as spt
import pandas as pd
import matplotlib.pyplot as plt
#import plotly
#import plotly.graph_objs as go
#import plotly.io as pio
import plotly.express as px
####
## conda install -c plotly plotly_express
## conda install -c plotly python-kaleido
## conda install -c plotly plotly-orca

##-------------------------------------
################################
from src import import_data as imp 
from src import basics as bas
#from src import initialstates as ini
from src import RelVecs as rel
from src import TravelTime as tt
#from src import FinalStates as fs 
#from src import dataframes as dtf
from src import molec_data as mda
#########################################
  
  
PotDir="ADFPotentials/"
FigureDepo="Figures/"
EivecDepo="EivecDepo/"
OutDepo="output/"
TimeDepo="TimeDepo/"
bas.directory_check_create(TimeDepo,clear=False)
bas.directory_check_create(EivecDepo,clear=False)
## abro ambos, sumo Coul y XC.
#fileGridLs0 = [direGeo+f for f in listdir(direGeo) if isfile(join(direGeo, f)) if fileGeoBas in f ]
fileInpLs = [PotDir+f for f in listdir(PotDir) if isfile(join(PotDir, f)) ]
print(fileInpLs)
InpDictio={}
for fi in fileInpLs:
  if "XC" in fi:
    XCinp=imp.ImportedData(fi)
    InpDictio["XC"]=XCinp
  if "Coul" in fi:
    Coulinp=imp.ImportedData(fi)
    InpDictio["Coul"]=Coulinp


print(Coulinp.PerAtomicSpecies)
#import sys 
#sys.exit("me fui")
print(XCinp.VtabFlat[0:18])
      
#mild check de que tengo los atomos apropiados.

assert(len(Coulinp.AtCatalog)==len(XCinp.AtCatalog) ) 
#### loop en los AtCatalog para evaluar distancias relativas. 
### Selec con preponderancia a las que tengan un origen en CTR o AX?
### how? Por rcyl==0 o < tol, sabemos que son CTR o AX. 
### con |H| < tol, sabemos si son CTR. 

for dct in Coulinp.AtCatalog:
  #print(dct)
  print(dct["index"],dct["species"],dct["special"],dct["car"])
  

FullPot=Coulinp.Vtab+XCinp.Vtab
Vinterp=spi.RegularGridInterpolator(Coulinp.GridCoordVecs ,FullPot) 

#print(Coulinp.AtCatalog)
print("====================================")
print("Making relative vectors")
n09=len(Coulinp.AtCatalog)
print("Expecting "+str(n09)+"x"+str(n09-1)+" = "+str(n09*(n09-1))   )
print("Expecting "+" "+str(n09*(n09-1)//2)   )
print("====================================")
distLs=[];
BigResoEnergLs=[];
MolecDire=Coulinp.MolecName
bas.directory_check_create(EivecDepo+MolecDire,clear=True)  
bas.directory_check_create(TimeDepo+MolecDire,clear=True)
for ivec, vec in enumerate(Coulinp.AtCatalog):
  for jvec, vecp in enumerate(Coulinp.AtCatalog):
    #print("ivec,jvec",ivec,jvec)
    if jvec<=ivec: ### salteamos los inversos...
      continue
    print("ivec,jvec",ivec,jvec)
    #print(vec["car"],vecp["car"])
    #difv=vec["car"]-vecp["car"]
    difv=rel.rel_vecs(vec["car"],vecp["car"],vec["index"],vecp["index"]\
      ,start_cyl=vec["IRadCyl"], end_cyl=vecp["IRadCyl"])
    print("indices ", vec["index"],vecp["index"])
    
    difv.closest_perp_dist_to_atom(Coulinp)
    difv.make_interpolated_potential(Vinterp)
    difv.calc_eigenstates()
    difv.calc_statdensity()
    #dirname="EivecDepo/relvec-"+str(ivec).zfill(3)+"-"+str(jvec).zfill(3)+"/"
    dirname=EivecDepo+MolecDire+"/relvec-"+str(vec["index"]).zfill(3)+"-"+str(vecp["index"]).zfill(3)+"/"
    bas.directory_check_create(dirname)
    np.savetxt( dirname+"Vtot.dat", np.transpose( [np.real(difv.grd0),np.real(difv.Vgrid0) ]) )
    np.savetxt( dirname+"Energ.dat", np.real( difv.Eisolved.BndEner) )
    np.savetxt( dirname+"Twidth.dat", np.transpose( \
          [np.real( difv.Eisolved.BndEner),np.real(difv.Eisolved.TWidth) ]) )
    np.savetxt( dirname+"StateDens.dat", \
             np.transpose( [difv.Energrid ,difv.dens]))
    for iEn,Ebnd in enumerate( difv.Eisolved.BndEner):
      np.savetxt( dirname+"psi-"+str(iEn).zfill(3), np.transpose( [np.real(difv.grd0),\
                            np.real(difv.Eisolved.bndStates[:,iEn]) ]) )
        
    difv.calc_classicaltime()
  
    np.savetxt(TimeDepo+MolecDire+"/ClassTime-"+str(vec["index"]).zfill(3)+"-"+str(vecp["index"]).zfill(3) \
             ,np.transpose( [difv.Energrid ,difv.TauVecClass ]) )
    difv.calc_semiclassicaltime()
    np.savetxt(TimeDepo+MolecDire+"/SClassTime-"+str(vec["index"]).zfill(3)+"-"+str(vecp["index"]).zfill(3) \
             ,np.transpose( [difv.Energrid ,difv.TauVecSClass ]) )
    #d=np.sqrt( np.dot(difv,difv) )
    #sss="{0:.6g}".format(d)
    #if sss in dls:
    #  continue
    #if sss=="0":
    #  continue
    #dls.append(sss)
    #aux=[ vec["species"],vecp["species"], "{0:.6g}".format(d) ]
    distLs.append(difv)
    BigResoEnergLs+=difv.ResoEnLs


#############################################################
withCTR=False
for dct in Coulinp.AtCatalog:
  if "CTR" or "AX" in dct["special"]:
    withCTR=True

pick_deepest_center=True
if pick_deepest_center==True and withCTR==True:
  deepenLs=[]
  ideepest=-21
  Vint0=1000.0
  for idv, difv in enumerate(distLs):
    if difv.outermost_cyl == 1 and min(difv.start_cyl,difv.end_cyl)==0:
      deepenLs.append(idv)#]=difv.
      if difv.Vintegral < Vint0:
        ideepest=idv
        Vint0=difv.Vintegral
####-----------------------------
  for idv in deepenLs:
    distLs[idv].ResoEnLs=distLs[ideepest].ResoEnLs
    #if difv.outermost_cyl <= 1 and min(difv.start_cyl,difv.end_cyl)==0:
      
      #deepenLs.append(idv)#]=difv.
      #if difv.Vintegral < Vint0:
      #  ideepest=idv
      #  Vint0=difv.Vintegral


###########################################################
        
EnergridGlob=distLs[-1].Energrid
BigRELs=np.real( np.asarray( sorted(BigResoEnergLs)) )
np.savetxt(EivecDepo+MolecDire+"ResoEners_eV.dat", BigRELs)
BigDens=rel.calc_statdensity(EnergridGlob,BigRELs)
np.savetxt(EivecDepo+MolecDire+"StatesDens_eV.dat", np.transpose([ EnergridGlob , BigDens ]))
#TWidths=1.0/ini.InitialStates.make_enerwidths(BigRELs)*bas.fs_to_au*1000.0*bas.eV_to_au

#np.savetxt("TWidths.dat",np.transpose([ BigRELs , TWidths ]))
PDBRELs=pd.DataFrame({"Resonant Energies [ev]": BigRELs })
PDBRELs.plot.hist(bins=100,range=(10.0,70.0))
plt.savefig(FigureDepo+'ResoEner.png')
##InitialStates(U,grid,bc="box",nsten=3,calcBnd=True,nEn=30)

bas.directory_check_create("TravelTimeMCtr/",clear=False)
TimeCombo=tt.TravelTime(distLs,Coulinp,NintermMX=5,TTdir="TravelTimeMCtr/"+MolecDire+"/" )
TimeCombo.make_combitos(distLs)
TimeCombo.make_travel_time_combo(distLs)
  #### Options... 1) leer estados? 2) generar estados y export.

WantedDF=pd.read_csv("data/Wanted.dat",sep=",")
del WantedDF["KinEeV"]
WantedDF.drop_duplicates(inplace=True)
##cluster,orb,thdeg,phideg,KinEeV
print(WantedDF.columns)
WantedDF.to_csv(OutDepo+"Wanted"+".out",index=True)
print(WantedDF)
#WantedDF["Ebnd"]=WantedDF["orb"]#.apply()
#for clu in mda.BndEnergs.keys():
WantedDF["Ebnd"]=WantedDF["orb"].replace(mda.BndEnergs)
WantedDF["KappaBnd"]=WantedDF["orb"].replace(mda.BndKappa)
#BigDF.set_index(["cluster"])
#BigDF=pd.DataFrame()#{"W4fCTR":[], "Se3dAX":[],"Se4sAX":[]}
BigDF=WantedDF.loc[WantedDF["cluster"]==Coulinp.MolecName].copy(deep=True)
print("--------------------------")
print( BigDF)

#nba.loc[nba["fran_id"] == "Lakers", "team_id"]
#EmDirLs={"Normal":,"W-Se":}#,"W-Gap"]
#OrbLs={"Normal":{"W4fCTR":[22.0,40], "Se3dAX":[],"Se4sAX":[70.0]},\
#       "W-Se":}
#["Normal","W4fCTR","W3Se2"]
BigDF.set_index(["cluster","orb","thdeg","phideg"])
BigDF["th"]=BigDF["thdeg"]*np.pi/180.0
BigDF["phi"]=BigDF["phideg"]*np.pi/180.0
Ndf=len(BigDF.columns)
Nrows=len(BigDF[BigDF.columns[0]])

print("Ndf,Nrows", Ndf,Nrows)
BigDF.set_index(pd.Index(np.arange(Nrows) ),inplace=True) 

thphPairs=[]
for idx in np.arange(Nrows):
  thphPairs.append([BigDF["thdeg"].iloc[idx],BigDF["phideg"].iloc[idx]])
thphPairs=bas.elimreps_sublist(thphPairs) #elimreps_sublist(lst)
print("thphPairs",thphPairs)
#import sys 
#sys.exit("me fui")
print( BigDF)
print(Coulinp.MolecName)
#import sys
#sys.exit("me fui")
#
spelist=[]
for cat in Coulinp.AtCatalog:
  if cat["special"]!=[]:
    for ct in cat["special"]:
      if ct not in spelist and ct!="TOP":#spelist=cat[]
        spelist.append(ct)
print("spelist", spelist)
CTRAXpos={}
for sp in spelist:
  CTRAXpos[sp]=[]
for cat in Coulinp.AtCatalog:
  for sp in spelist:
    if sp in cat["special"] :
      CTRAXpos[sp].append(cat["car"])
#  elif cat["special"]=="AX":
#    CTRAXpos.append(cat["car"])
print("CTRAXpos", CTRAXpos)
  
EmDirDictio={"emdir":[]}
SpecDictio={"special":[]}
PosDictio={"position":[]}
#KinEeVTol={"KinEeVTol":[]}
SpeciesDictio={"species":[]}
#AtomSpecial={}
for idx in np.arange(Nrows):
  costh=np.cos(BigDF["th"].iloc[idx] )
  sinth= np.sin(BigDF["th"].iloc[idx] )
  cosph=np.cos(BigDF["phi"].iloc[idx])
  sinph=np.sin(BigDF["phi"].iloc[idx])
  EmDirDictio["emdir"].append(np.array([ sinth*cosph,sinth*sinph ,costh ]) )
  
  sp=mda.molecSpecial[BigDF["cluster"][idx]]
  SpecDictio["special"].append(sp )
  PosDictio["position"].append( CTRAXpos[sp] )
  
  #KinEeVTol["KinEeVTol"].append(5.0)#
  #print( BigDF["cluster"][idx] )
  
#for EmDir in EmDirLs:
BigDF["emdir"]=EmDirDictio["emdir"]#np.zeros(3,dtype=float)*BigDF["phi"]
BigDF["special"]=SpecDictio["special"]
BigDF["position"]=PosDictio["position"]
#BigDF["KinEeVTol"]=KinEeVTol["KinEeVTol"]
#for Orb in OrbLs:

#### loopazo en distLs, en los eivals.
###
replaDictio={}#pd.DataFrame()
for col in BigDF.columns:
  replaDictio[col]=[]
dminDct={"dmin":[]}
dmaxDct={"dmax":[]}
vlen={"vlen":[]}
DIDct={"dint":[]}
wt00Dict={"wt00":[]}
DensInt={"DensInt":[]}
ElistDct={"Elist":[]}
DelaySCdct={"delay_sc":[]}
#DeltaEListDict={"DeltaE":[]}
AngDct={"angsep":[]}
segStart={"StartNode":[]}
segEnd={"EndNode":[]}
cylStart={"start_cyl":[]}
cylEnd={"end_cyl":[]}
#isTop={"is_top":[]}
CSradDict={"cs_rad":[]}
speciesDct={"speciesLs":[]}
#import sys 
#sys.exit("me fui")
for idx in np.arange(Nrows):
  #E=BigDF["KinEeV"].iloc[idx]
  #Etol=BigDF["KinEeVTol"].iloc[idx]
  for difv in distLs:
    gotEnerg=False
    ResoNP=np.asarray(difv.ResoEnLs)
    imx=np.argmin( np.abs(ResoNP-120.0) )
    evec=ResoNP[0:imx+1]## difv.ResoEnLs[0:imx]#np.where( ResoNP<120.0, ResoNP )  
    DelaySC=difv.TauVecSClass
    if len(evec)>0:
      gotEnerg=True
    if gotEnerg ==True:
      for ier,er in enumerate(evec):
        for col in BigDF.columns:
          replaDictio[col].append(BigDF[col].iloc[idx])
        dmin,dmax=bas.calc_distance_to_ref(BigDF["position"][idx], [difv.vec0,difv.vecf])
        DInt=bas.get_integrated_distance(BigDF["position"][idx],difv.vec0,difv.vecf)
        wt00D=bas.get_integrated_distance_parab(BigDF["position"][idx],difv.vec0,difv.vecf,BigDF["KappaBnd"][idx])
        dminDct["dmin"].append(dmin)
        dmaxDct["dmax"].append(dmax)
        vlen["vlen"].append(difv.vlen)
        DIDct["dint"].append(DInt)
        wt00Dict["wt00"].append(wt00D)
        DNS=bas.get_integrated_dens_parab(BigDF["position"][idx],difv.vec0,difv.vecf,BigDF["KappaBnd"][idx])
        DensInt["DensInt"].append(DNS)
        cosang=abs(bas.cosgamma(BigDF["emdir"].iloc[idx],difv.vecrel))
        AngDct["angsep"].append(cosang)
        CSradDict["cs_rad"].append(difv.CSradius)
        segStart["StartNode"].append(difv.indexLs[0])
        segEnd["EndNode"].append(difv.indexLs[1] )
        sp1=Coulinp.AtCatalog[difv.indexLs[0]]["species"] ;
        sp2=Coulinp.AtCatalog[difv.indexLs[1]]["species"];
        cylStart["start_cyl"].append( Coulinp.AtCatalog[difv.indexLs[0]]["IRadCyl"] )
        cylEnd["end_cyl"].append( Coulinp.AtCatalog[difv.indexLs[1]]["IRadCyl"] )
        lsp=[sp1,sp2];
        lsp=sorted(lsp)
        
        speciestring=lsp[0]+"-"+lsp[1]
                
        speciesDct["speciesLs"].append( speciestring )
        ElistDct["Elist"].append(np.real(er) )#:[]}
        DelaySCdct["delay_sc"].append(DelaySC[ier])
        #DeltaEListDict["DeltaE"].append(np.real(deltaE))
        ## limitar a LA energ mas proxima.
HugeDF=pd.DataFrame(replaDictio)
HugeDF["allignment"]=AngDct["angsep"]
HugeDF["startnode"]=segStart["StartNode"]
HugeDF["endnode"]=segEnd["EndNode"]
HugeDF["start_cyl"]=cylStart["start_cyl"]
HugeDF["end_cyl"]=cylEnd["end_cyl"]
HugeDF["outermost_cyl"]=np.where(HugeDF["start_cyl"]>= HugeDF["end_cyl"],\
                                 HugeDF["start_cyl"] , HugeDF["end_cyl"] )
LargestCyl=HugeDF["outermost_cyl"].max()
HugeDF["am_i_outermost"]=np.where( HugeDF["outermost_cyl"]==LargestCyl,1,0 )
HugeDF["species"]=speciesDct["speciesLs"]
HugeDF["Elist"]=ElistDct["Elist"]
HugeDF["delay_sc"]=DelaySCdct["delay_sc"]
#HugeDF["DElist"]=DeltaEListDict["DeltaE"]
HugeDF["dmin"]=dminDct["dmin"]
HugeDF["dmax"]=dmaxDct["dmax"]
HugeDF["dint"]=DIDct["dint"]
HugeDF["vlen"]=vlen["vlen"]
HugeDF["cs_rad"]=CSradDict["cs_rad"]
HugeDF["wtBnd"]=wt00Dict["wt00"]

print(HugeDF["wtBnd"].max())
HugeDF["wtBnd"]=HugeDF["wtBnd"]#/HugeDF["wtBnd"].max() #renorm
HugeDF["DensInt"]=DensInt["DensInt"]
#HugeDF["DensInt"]=HugeDF["DensInt"]#/HugeDF["DensInt"].max
#HugeDF["is_top"]=np.where( "TOP" in HugeDF["special"],True,False)
#print(HugeDF.describe(include=np.object))
print(HugeDF.describe())#include=np.object))
## me quito los elementos objetos/arrays y demas yerbas.

#### Weighing options for the density of states ...
HugeDF["wt01"]=HugeDF["allignment"]**2*np.exp(-HugeDF["KappaBnd"]*HugeDF["dint"] )
HugeDF["wt02"]=HugeDF["allignment"]**2*\
  (  np.exp(-HugeDF["KappaBnd"]*HugeDF["dmin"]) - np.exp(-HugeDF["KappaBnd"]*HugeDF["dmax"]) )\
    /HugeDF["KappaBnd"]
HugeDF["wt03"]=HugeDF["allignment"]*np.exp(-HugeDF["KappaBnd"]*HugeDF["dint"] )
HugeDF["wt04"]=HugeDF["allignment"]*\
  (  np.exp(-HugeDF["KappaBnd"]*HugeDF["dmin"]) - np.exp(-HugeDF["KappaBnd"]*HugeDF["dmax"]) )\
    /HugeDF["KappaBnd"]
HugeDF["wt05"]=1.0+HugeDF["allignment"]**2*\
  (  np.exp(-HugeDF["KappaBnd"]*HugeDF["dmin"]) - np.exp(-HugeDF["KappaBnd"]*HugeDF["dmax"]) )\
    /HugeDF["KappaBnd"]
HugeDF["wt06"]=1.0+HugeDF["allignment"]*\
  (  np.exp(-HugeDF["KappaBnd"]*HugeDF["dmin"]) - np.exp(-HugeDF["KappaBnd"]*HugeDF["dmax"]) )\
    /HugeDF["KappaBnd"]
HugeDF["wt06"]=(  np.exp(-HugeDF["KappaBnd"]*HugeDF["dmin"]) - np.exp(-HugeDF["KappaBnd"]*HugeDF["dmax"]) )\
    /HugeDF["KappaBnd"]
#HugeDF["wt06"]=
#  (  np.exp(-HugeDF["KappaBnd"]*HugeDF["dmin"]) - np.exp(-HugeDF["KappaBnd"]*HugeDF["dmax"]) )\
#    /HugeDF["KappaBnd"]
wtLs=[col for col in HugeDF.columns if "wt" in col]
for iwt in wtLs:
  striwt=str(iwt).zfill(2)
  mkey="wt"+striwt
  
HugeDFflt=HugeDF.filter(["cluster","orb","Ebnd","KappaBnd","thdeg",\
    "phideg","Elist","allignment","dmin","dmax","dint",\
      "species","outermost_cyl","am_i_outermost","wt06","wtBnd","DensInt","vlen","cs_rad"])
# fig = plt.figure(figsize=(10, 7.5), dpi=80)
# fig.set_frameon(False)
HugeDFflt["cs_rad_vlen"]=HugeDFflt["cs_rad"]/HugeDFflt["vlen"]
# ax1 = fig.add_subplot(111)

# ax1.set_ylabel('Time delay (as)', fontsize = 35)
# ax1.set_xlabel('Photon Energy (eV)', fontsize = 35)
replaLs=HugeDFflt.species.unique()
print(replaLs)
NSymbol=len(replaLs)
markercatalog=["circle","square","diamond","triangle","triangle-left","triangle-right"]
autoMarker=False
mysymbolmap={}
if autoMarker==True:
  temp=HugeDFflt.species
  for ire,repla in enumerate(replaLs,0):
    temp=temp.replace(repla,markercatalog[ire])
    mysymbolmap[repla]=markercatalog[ire]
  #.replace("two","circle")
else:
  temp=HugeDFflt.species
  #for ire,repla in enumerate(replaLs,0):
  temp=temp.replace("Se-W",markercatalog[0])
  mysymbolmap["Se-W"]=markercatalog[0]
  temp=temp.replace("W-W",markercatalog[1])
  mysymbolmap["W-W"]=markercatalog[1]
  temp=temp.replace("Se-Se",markercatalog[2])
  mysymbolmap["Se-Se"]=markercatalog[2]
#temp
#print(markervec)
#import sys
#sys.exit("me fui")
CylRadColor=["r","b","g"]
#layout = dict()
for Orb in HugeDFflt.orb.unique():
  
  
  
  EKmin=mda.orbEKminEKmax[Orb][0]
  EKmax=mda.orbEKminEKmax[Orb][1]
  for thph in thphPairs:
    strname=Coulinp.MolecName+"_"+Orb\
        +"_"+str(int(thph[0])).zfill(3)+"_"+str(int(thph[1])).zfill(3)+"_AllEn"
    condition=(HugeDFflt["thdeg"]==thph[0]) & \
        (HugeDFflt["phideg"]==thph[1])\
            & (HugeDFflt["orb"]==Orb) & (HugeDFflt["dmin"]<3.0/HugeDFflt["KappaBnd"])  \
              & (HugeDFflt["cs_rad_vlen"]>0.1)  
    HugeDFflt2=HugeDFflt.loc[condition ]
    markervec=temp.loc[condition ]
    if len(HugeDFflt2.index)==0:
      continue
    HugeDFflt2.to_csv(OutDepo+strname+".dat")
    
    HugeDFflt2["markers"]=markervec#["species"]
    HugeDFflt2["DensInt"]=HugeDFflt2["DensInt"]/(HugeDFflt2["DensInt"].max())
    HugeDFflt2["wtBnd"]=HugeDFflt2["wtBnd"]#/(HugeDFflt2["wtBnd"].max())
    HugeDFflt2["wt06"]=HugeDFflt2["wt06"]/(HugeDFflt2["wt06"].max())
#  HugeDFflt.loc[ HugeDFflt["thdeg"]==thph[0]].to_csv(Coulinp.MolecName\
#      +"_"+str(thph[0])+"_"+str(thph[1])+"dat")
#  HugeDFflt.loc[ HugeDFflt["phideg"]==thph[1]].to_csv(Coulinp.MolecName\
#      +"_"+str(thph[0])+"_"+str(thph[1])+"dat")
    #ax1.plot(tdtab,COE,\#color=colorvec[icolor], \
#    ax1.plot(tdtab,COE,\linestyle="-", linewidth=4)
#    savefig(strname+".png")
    #dff = px.data.HugeDFflt()
    #markershape = HugeDFflt2["is_top"].replace(True,"square").replace(False,"circle")
    #range_color=[5,8]
    dintspread=HugeDFflt2["vlen"].std()
    dintmean=HugeDFflt2["vlen"].mean()
    dintmin=HugeDFflt2["vlen"].min()
    dintmax=HugeDFflt2["vlen"].max()
    #if dintspread< 0.1 :
    dintmin-=0.1
    dintmax+=0.1
    dintmin=bas.roundcyph(dintmin,1)
    dintmax=bas.roundcyph(dintmax,1)
    
    #else :
    #  dintmin=bas.roundcyph(dintmin,1)
    #  dintmax=bas.roundcyph(dintmax,1)
    ry=HugeDFflt2['allignment']#HugeDFflt2['vlen']#HugeDFflt2['allignment']
    rsize=3*(2+0*HugeDFflt2['allignment'] )   #6-0*HugeDFflt2["am_i_outermost"]
    rz=HugeDFflt2['allignment']
    rcolor=HugeDFflt2['cs_rad_vlen']#HugeDFflt2['allignment']#HugeDFflt2['vlen']#HugeDFflt2['dint']#HugeDFflt2['wt06']#HugeDFflt2['DensInt']
    ropa=0.5
    rsymbol=HugeDFflt2["species"]
    fig = px.scatter(HugeDFflt2, x=HugeDFflt2['Elist'],\
                  y=ry,size=rsize, color=rcolor,opacity=ropa,\
                      symbol=rsymbol,symbol_map=mysymbolmap,\
                labels={'Elist': "Kinetic Energy [eV]","vlen":"R.V. lenght [a.u.]",
        "dmin": "Minimal distance [a.u.]",
        "dint":"Dist. [a.u.]",'cs_rad_vlen':"Rel. Eff. rad.",
            "allignment": "Alignment", "species":"Atomic <br>species"}
                ,range_color=[0.0,1.0],
            #    title=Coulinp.MolecName+" - "+Orb+\
            #       " "+"("+str(thph[0])+" - "+str(thph[1])+")" \
            #title=Coulinp.MolecName+" - "+Orb+ " : "+r"$\theta=$ "+\
            #   str(thph[0]) +" "+r"$\phi$ "+str(thph[1])\
                width=800, height=400)#,\
                  #legend=dict(orientation="h")) #s### "markers":markercatalog[0:NSymbol]}
  
    fig.update_xaxes(range=[EKmin, EKmax],tick0=0.0, dtick=10.0)
    #fig.update_zaxes(range=[0.0, 1.0],tick0=0.0, dtick=0.20)
    fig.update_layout(coloraxis_colorbar=dict(
      title="Rel. Eff.  <br> rad.",#"Align. <br> ",#" <br>"+"dist. [a.u.]",#"Av. <br>"+"dist. [a.u.]",
      thicknessmode="pixels", thickness=20,
      lenmode="pixels", len=200,
      yanchor="bottom", y=0,
      ticks="outside", ticksuffix=" ",tickvals = [0.0, 0.5,1.0]))
    #fig.update_yaxes(range=[-0.1, 1.1])#tick0=0.25, dtick=0.5)
#     if dintspread > 0.1 :
#       print("Normal dint spread")
#       fig.update_yaxes(range=[-0.1, 1.1])#tick0=0.25, dtick=0.5)
#       #) )
#       #fig.update_yaxes(range=[ dintmin,dintmax])
#     else :
#       print("Low dint spread")
    fig.update_layout(autosize=True,margin=dict(l=20, r=60, t=20, b=20,pad=0),\
                    paper_bgcolor="White")#"LightSteelBlue")#,paper_bgcolor="LightSteelBlue")
    fig.update_yaxes(range=[-0.1, 1.1])
      #fig.update_yaxes(range=[ dintmin,dintmax])#tick0=0.25, dtick=0.5)
       #tickvals = [ dintmin,dintmax] )
        
#####       tickvals = [ round((dintmean-0.25)*10)/10,\
#####      round(dintmean*10)/10, round((dintmean+0.25)*10)/10]
        

    ###fig.update_yaxes(tickvals=[5.1, 5.9, 6.3, 7.5])
    ### fig.update_xaxes(tickangle=45, tickfont=dict(family='Rockwell', color='crimson', size=14))
    fig.update_layout(font=dict(size=22) )
    graphStr=strname+".png"
    fig.write_image(FigureDepo+graphStr)#show()
    # ---- ----
    ropa=0.8
    mycolormap={"0":"magenta","1": "red","2": "blue","3": "green","4": "yellow"}
    rcolor=HugeDFflt2["outermost_cyl"].astype(int).astype(str)
    myorder={"color": ["0", "1", "2", "3", "4"]}
    fig = px.scatter(HugeDFflt2, x=HugeDFflt2['Elist'],\
                  y=ry,size=rsize, color=rcolor,
                  opacity=ropa,\
                      symbol=rsymbol,symbol_map=mysymbolmap,\
                labels={'Elist': "Kinetic Energy [eV]","vlen":"R.V. lenght [a.u.]",
        "dmin": "Minimal distance [a.u.]",
        "dint":"Dist. [a.u.]",'cs_rad_vlen':"Rel. Eff. rad.","color":"Cyl. layer",\
          "allignment": "Alignment", "species":"<br>Atomic <br>species"},\
                range_color=[0.5,3.5],color_discrete_map=mycolormap,\
                  category_orders=myorder,\
                width=820, height=400)#,\
                  #legend=dict(orientation="h")) #s### "markers":markercatalog[0:NSymbol]}
  
    fig.update_xaxes(range=[EKmin, EKmax],tick0=0.0, dtick=10.0)
    #fig.update_zaxes(range=[0.0, 1.0],tick0=0.0, dtick=0.20)
    fig.update_layout(coloraxis_colorbar=dict(
      title="Cyl. <br> layer",#"Align. <br> ",#" <br>"+"dist. [a.u.]",#"Av. <br>"+"dist. [a.u.]",
      thicknessmode="pixels", thickness=20,
      lenmode="pixels", len=200,
      yanchor="bottom", y=0,
      ticks="outside", ticksuffix=" ",tickvals = [1,2,3]))
    fig.update_yaxes(range=[-0.1, 1.1])
    fig.update_layout(autosize=True,margin=dict(l=20, r=60, t=20, b=20,pad=0)\
                    ,paper_bgcolor="White")#"LightSteelBlue")#,paper_bgcolor="LightSteelBlue")
    ###fig.update_yaxes(tickvals=[5.1, 5.9, 6.3, 7.5])
    ### fig.update_xaxes(tickangle=45, tickfont=dict(family='Rockwell', color='crimson', size=14))
    fig.update_layout(font=dict(size=22) )
    graphStr2=strname+"Cyl.png"
    fig.write_image(FigureDepo+graphStr2)#show()
    
    mergedStr=FigureDepo+"Merged"+strname+".png"
    sbp.call("montage "+FigureDepo+graphStr+" "+FigureDepo+graphStr2 +\
             " -tile 1x2 -gravity West -geometry 830x400\>+2+0  "+mergedStr ,shell=True)
###############################################################3
##############################################################33
HugeDFflt=HugeDF.filter(["cluster","orb","Ebnd","thdeg",\
    "phideg","Elist","delay_sc","allignment",\
      "dmin","dmax","dint","species",\
        "wt01","wt02","wt03","wt04","wt05","wt06"])
print(HugeDFflt.filter(["wt02"]).head(12))
print(HugeDFflt.filter(["Elist"]).head(12))
EnerGrid=np.linspace(10.0,150.0,351)
densDictio={}
wtLs=[col for col in HugeDF.columns if "wt" in col if "Bnd" not in col]
#for iwt in wtLs:
#  striwt=str(iwt).zfill(2)
#  mkey="wt"+striwt

baseline=["TWbCore"]

for Orb in HugeDFflt.orb.unique():
  Dout="StatesDens/"+Coulinp.MolecName+"_"+Orb+"/"
  bas.directory_check_create(Dout)
  
  baselinefile=baseline[0]+"_"+Orb+".dat"
  for thph in thphPairs:
    dirbaseline="data/delaybaseline_"+str(int(thph[0])).zfill(3)+\
      "_"+str(int(thph[1])).zfill(3)
    
    bslfile=dirbaseline+"/"+baselinefile
    delayBaseline=np.loadtxt(bslfile)
    delayBaseline[:,0]=mda.BndEnergs[Orb]
    delayBLinterp=np.interp(EnerGrid,delayBaseline[:,0],delayBaseline[:,1])
    for iwt in wtLs:
      striwt=str(iwt).zfill(2)
      mkey=striwt
      condition=(HugeDFflt["thdeg"]==thph[0]) & \
          (HugeDFflt["phideg"]==thph[1])\
              & (HugeDFflt["orb"]==Orb)
      HugeDFflt2=HugeDFflt.loc[condition ]
      newkey=Coulinp.MolecName+"_"+Orb+"_"+str(thph[0])+"_"+str(thph[1])+"_"+mkey
      
      dens=rel.calc_weighted_statdensity(EnerGrid,\
                  HugeDFflt2["Elist"],HugeDFflt2["delay_sc"]*HugeDFflt2[mkey])
      densDictio[newkey]=dens
      np.savetxt(Dout+newkey+".dat",np.transpose([EnerGrid,dens+delayBLinterp]))

print("Finished with : ", Coulinp.MolecName)
